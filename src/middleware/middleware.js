// middleware.js
import { useRouter } from "next/router";
import { useEffect } from "react";

export default function withAuth(Component) {
  return function AuthenticatedComponent(props) {
    const router = useRouter();

    useEffect(() => {
      const token = localStorage.getItem("token"); // Đọc token từ localStorage hoặc cookie

      if (!token) {
        // Nếu không có token, chuyển hướng đến trang đăng nhập
        router.push("/login");
      }
    }, []);

    return <Component {...props} />;
  };
}
