const roles = [
  {
    id: 1,
    role: "Admin",
    value: "admin",
  },
  {
    id: 2,
    role: "Principle",
    value: "principle",
  },
  {
    id: 3,
    role: "Teacher",
    value: "teacher",
  },
  {
    id: 4,
    role: "Parent",
    value: "parent",
  },
];

export default roles;
