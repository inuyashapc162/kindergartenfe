import studentIcon from "../../public/img/customer_module.png";
import healthIcon from "../../public/img/healthcare_module.png";
import attendanceIcon from "../../public/img/attendance_module.png";

const menu_parent = [
  {
    id: 1,
    module: "Student",
    img: studentIcon,
    link: "/student",
  },
  {
    id: 2,
    module: "Health report",
    img: healthIcon,
    link: "/healthcare",
  },
  {
    id: 3,
    module: "Attendance",
    img: attendanceIcon,
    link: "/attendance",
  },
];

export default menu_parent;
