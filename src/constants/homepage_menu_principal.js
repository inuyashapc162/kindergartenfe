import classIcon from '../../public/img/class_module.png';
import teacherIcon from '../../public/img/teacher_module.png';
import mealIcon from '../../public/img/nutrition_module.png';

const menu_principal = [
  {
    id: 1,
    module: "Class",
    img: classIcon,
    link: '/principal/class',
  },
  {
    id: 2,
    module: "Teacher",
    img: teacherIcon,
    link: '/principal/teacher',
  },
  {
    id: 3,
    module: "Menu",
    img: mealIcon,
    link: '/menu',
  },
];

export default menu_principal;
