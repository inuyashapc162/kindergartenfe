export const EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
export const PHONE_NUMBER_REGEX = /^(?:\+84|84|0)[35789]\d{8}$/g;
