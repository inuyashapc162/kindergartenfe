import studentIcon from "../../public/img/customer_module.png";
import attendanceIcon from "../../public/img/attendance_module.png";
import educationPlanIcon from "../../public/img/education_plan_module.png";
import healthIcon from "../../public/img/healthcare_module.png";
import mealReportIcon from "../../public/img/eats_module.png";
import milkReportIcon from "../../public/img/milks_module.png";

const menu_teacher = [
  {
    id: 1,
    module: "Student Manage",
    img: studentIcon,
    link: "/student",
  },
  {
    id: 2,
    module: "Attendance",
    img: attendanceIcon,
    link: "/attendance",
  },
  {
    id: 3,
    module: "Education plan",
    img: educationPlanIcon,
    link: "/curriculum/plan",
  },
  {
    id: 4,
    module: "Health report",
    img: healthIcon,
    link: "/healthcare",
  },
  {
    id: 5,
    module: "Meal report",
    img: mealReportIcon,
    link: "/meal-report",
  },
  {
    id: 6,
    module: "Milk report",
    img: milkReportIcon,
    link: "/milk-report",
  },
];

export default menu_teacher;
