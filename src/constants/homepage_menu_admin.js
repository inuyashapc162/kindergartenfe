import accountIcon from '../../public/img/account_module.png';
import schoolIcon from '../../public/img/school-icon.png';
const menu_admin = [
  {
    id: 1,
    module: "Account",
    img: accountIcon,
    link: '/admin/listAccount',
  },
  {
    id: 2,
    module: "School",
    img: schoolIcon,
    link: '/admin/school',
  },
];

export default menu_admin;
