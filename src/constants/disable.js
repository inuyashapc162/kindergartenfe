const disabilities = [
    {
        id: 1,
        value: "Mobility impairment",
    },
    {
        id: 2,
        value: "Physical deformity",
    },
    {
        id: 3,
        value: "Visual impairment",
    },
    {
        id: 4,
        value: "Hearing impairment",
    },
    {
        id: 5,
        value: "Speech and language impairment",
    },
    {
        id: 6,
        value: "Intellectual disability",
    },
    {
        id: 7,
        value: "Psychiatric or mental health disability",
    },
    {
        id: 8,
        value: "Other",
    },
    {
        id: 9,
        value: "None",
    }
];

export default disabilities;