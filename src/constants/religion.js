const religion = [
    {
        id: 1,
        value: "Buddhism"
    },
    {
        id: 2,
        value: "Catholicism"
    },
    {
        id: 3,
        value: "Islam"
    },
    {
        id: 4,
        value: "Other",
    },
    {
        id: 5,
        value: "None",
    },
];

export default religion;