const status = [
    {
        id: 1,
        value: "Studying"
    },
    {
        id: 2,
        value: "Leave of absence"
    },
    {
        id: 3,
        value: "Drop out"
    }
];

export default status;