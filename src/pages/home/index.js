import Image from "next/image";
import Link from "next/link";
import menu_principal from "@/constants/homepage_menu_principal";
import menu_admin from "@/constants/homepage_menu_admin";
import homepageBackground from "../../../public/img/education_homepage_background.png";
import menu_teacher from "@/constants/homepage_menu_teacher";
import decodeToken from "@/utils/auth";
import Logo from "../../../public/img/logo-school.png";
import { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import menu_parent from "@/constants/homepage_menu_parent";

export default function Home() {
  const router = useRouter();

  const [schoolName, setSchoolName] = useState();
  const [username, setUsername] = useState();
  const [role, setRole] = useState();
  const [id, setId] = useState();
  const getInfoHomepage = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    console.log("🚀 ========= decode:", decode?.decoded);
    setUsername(JSON.parse(token)?.username);
    setRole(decode?.decoded?.data?.user?.role?.roleName);
    setId(decode?.decoded?.data?.user?._id);
    try {
      const result = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/schools/${decode?.decoded?.data?.user?.schoolID}`
      );
      setSchoolName(result?.data?.data?.schoolName);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  // const getInfoHomepage = async () => {
  //   const token = localStorage.getItem("token");
  //   console.log("🚀 ========= token1234:", JSON.parse(token));
  //   const decode = decodeToken(JSON.parse(token)?.token);
  //   console.log("🚀 ========= decode:", decode);
  //   setUsername(JSON.parse(token)?.username);
  //   setRole(JSON.parse(token)?.user?.role?.roleName);

  //   try {
  //     const result = await axios.get(
  //       `${process.env.NEXT_PUBLIC_API_URL}/schools/${decode?.data?.user?.schoolID}`
  //     );
  //     setSchoolName(result?.data?.data?.schoolName);
  //   } catch (error) {
  //     console.log("🚀 ========= error:", error);
  //   }
  // };

  const handleLogout = () => {
    localStorage.removeItem("token");
    router.push("/");
  };

  useEffect(() => {
    getInfoHomepage();
  }, []);

  return (
    <div
      className="flex flex-col min-h-screen bg-cover bg-center h-screen"
      style={{ backgroundImage: `url(${homepageBackground.src})` }}
    >
      <div className="bg-gray-800 bg-opacity-50 py-1">
        <div className="container mx-auto px-4">
          <nav className="flex items-center justify-between">
            <div className="flex items-center">
              <div className="mr-4">
                <Image width={150} height={50} src={Logo} alt="active" />
              </div>
              <ul>
                <li>
                  <span className="text-white uppercase font-bold">
                    {schoolName}
                  </span>
                </li>
              </ul>
            </div>
            <div className="flex items-center">
              <ul className="mr-4 flex">
                <li>
                  <Link
                    href={`admin/changeProfile/${id}`}
                    className="text-white text-lg"
                  >
                    {username}
                  </Link>
                </li>
              </ul>
              <ul className="flex">
                <div className="flex" onClick={handleLogout}>
                  <div className="flex py-2 relative px-2 gap-1 text-center rounded-md">
                    <a
                      className="flex rounded-md text-3xl align-middle relative"
                      href="#"
                    >
                      <i className="fa fa-sign-out text-white bg-gray-800 rounded-full p-2"></i>
                    </a>
                  </div>
                </div>
              </ul>
            </div>
          </nav>
        </div>
      </div>

      <div className="mt-10 flex-grow">
        <div>
          <div className="top-title">
            <div className="font-extrabold text-lg text-center">
              CHỌN CHUYÊN MỤC QUẢN LÝ
            </div>
          </div>
        </div>
        <div className="flex flex-wrap mx-auto w-4/5">
          {role === "Principal" &&
            menu_principal.map((item) => (
              <div key={item.id} className="w-full md:w-1/2 lg:w-1/4 p-4">
                <div className="mb-4 mx-auto flex flex-col items-center justify-center bg-white rounded-2xl py-[25px] w-[130px] hover:bg-gradient-to-bl from-blue-500 via-blue-400 to-blue-300">
                  <Link href={item?.link}>
                    <Image
                      width={80}
                      height={80}
                      src={item?.img}
                      alt={`${item.module} Icon`}
                    />
                  </Link>
                </div>
                <div className="mx-auto text-center uppercase text-[12px] font-extrabold">
                  <Link href={item?.link}>{item?.module}</Link>
                </div>
              </div>
            ))}
          {role === "Teacher" &&
            menu_teacher.map((item) => (
              <div key={item.id} className="w-full md:w-1/2 lg:w-1/4 p-4">
                <div className="mb-4 mx-auto flex flex-col items-center justify-center bg-white rounded-2xl py-[25px] w-[130px] hover:bg-gradient-to-bl from-blue-500 via-blue-400 to-blue-300">
                  <Link href={item?.link}>
                    <Image
                      width={80}
                      height={80}
                      src={item?.img}
                      alt={`${item.module} Icon`}
                    />
                  </Link>
                </div>
                <div className="mx-auto text-center uppercase text-[12px] font-extrabold">
                  <Link href={item?.link}>{item?.module}</Link>
                </div>
              </div>
            ))}
          {role === "Admin" &&
            menu_admin.map((item) => (
              <div key={item.id} className="w-full md:w-1/2 lg:w-1/4 p-4">
                <div className="mb-4 mx-auto flex flex-col items-center justify-center bg-white rounded-2xl py-[25px] w-[130px] hover:bg-gradient-to-bl from-blue-500 via-blue-400 to-blue-300">
                  <Link href={item?.link}>
                    <Image
                      width={80}
                      height={80}
                      src={item?.img}
                      alt={`${item.module} Icon`}
                    />
                  </Link>
                </div>
                <div className="mx-auto text-center uppercase text-[12px] font-extrabold">
                  <Link href={item?.link}>{item?.module}</Link>
                </div>
              </div>
            ))}
          {role === "Parent" &&
            menu_parent.map((item) => (
              <div key={item.id} className="w-full md:w-1/2 lg:w-1/4 p-4">
                <div className="mb-4 mx-auto flex flex-col items-center justify-center bg-white rounded-2xl py-[25px] w-[130px] hover:bg-gradient-to-bl from-blue-500 via-blue-400 to-blue-300">
                  <Link href={item?.link}>
                    <Image
                      width={80}
                      height={80}
                      src={item?.img}
                      alt={`${item.module} Icon`}
                    />
                  </Link>
                </div>
                <div className="mx-auto text-center uppercase text-[12px] font-extrabold">
                  <Link href={item?.link}>{item?.module}</Link>
                </div>
              </div>
            ))}
        </div>
      </div>
      <footer className="bg-gray-800 bg-opacity-50 py-4 mt-auto">
        <div className="container mx-auto flex justify-between items-center">
          <div className="mr-4">
            <Image width={150} height={50} src={Logo} alt="active" />
          </div>
          <div className="text-white text-right">
            <strong>
              Copyright © 2024{" "}
              <a href="link trang home" className="text-teal-400">
                {schoolName}&nbsp;
              </a>
            </strong>
            All rights reserved.
          </div>
        </div>
      </footer>
    </div>
  );
}
