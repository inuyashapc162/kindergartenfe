import Navbar from "@/components/Navbar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { MenuItem, Select } from "@mui/material";
import { faFloppyDisk } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { DatePicker } from "@mui/x-date-pickers";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";
import stringifyUrl from "@/utils/stringifyUrl";

export default function Page({
  report,
  totalNumMilkreport,
  totalNumMilks,
  totalHaveMilkOfSelectedDate,
  totalNotHaveMilkOfSelectedDate,
  query,
}) {
  const [classes, setClasses] = useState([]);
  const [isChange, setIsChange] = useState(false);
  const [selectAllValue, setSelectAllValue] = useState();
  const [map, setMap] = useState(
    new Map(
      report?.map((item) => [
        item.milkreportId,
        Number(item.haveMilkOfSelectedDate),
      ]),
    ),
  );

  const fetchClasses = async () => {
    const res = await axios.get(process.env.NEXT_PUBLIC_API_URL + `/classes`, {
      headers: {
        token: `${JSON.parse(localStorage.getItem("token"))?.token} `,
        Authorization: `${JSON.parse(localStorage.getItem("token"))?.token} `,
      },
    });
    setClasses(res?.data?.data);
    router.push(
      `/milk-report?${stringifyUrl({
        ...query,
        classId: res?.data?.data[0]?._id,
        date: dayjs().format("YYYY-MM-DD"),
      })}`,
      undefined,
      { shallow: false },
    );
  };

  const router = useRouter();

  console.log(router.query);

  useEffect(() => {
    fetchClasses();
  }, []);

  useEffect(() => {
    setMap(
      new Map(
        report?.map((item) => [
          item.milkreportId,
          Number(item.haveMilkOfSelectedDate),
        ]),
      ),
    );
  }, [report]);

  const handleRadioChange = (e, item) => {
    setIsChange(true);
    setMap(new Map(map.set(item.milkreportId, Number(e.target.value))));
  };

  const handleSelectAll = (value) => {
    setIsChange(true);
    setSelectAllValue(value);
    report?.forEach((item) => {
      setMap(new Map(map.set(item.milkreportId, Number(value))));
    });
  };

  const handleSave = async () => {
    const data = Array.from(map.keys()).map((key) => ({
      milkreportId: key,
      haveMilk: map.get(key),
      date: dayjs(query.date).format("YYYY-MM-DD"),
    }));

    try {
      const res = await axios.put(
        process.env.NEXT_PUBLIC_API_URL + `/milk-report`,
        data,
      );

      if (res) {
        toast.success("Lưu thay đổi thành công");
      }
    } catch (error) {
      toast.error("Lưu thay đổi thất bại");
    }
  };

  const handleSelectClass = (e) => {
    const queryStringify = stringifyUrl({ ...query, classId: e.target.value });
    router.push(`/milk-report?${queryStringify}`);
  };

  const handleSelectDate = (value) => {
    const queryStringify = stringifyUrl({
      ...query,
      date: dayjs(value).format("YYYY-MM-DD"),
    });
    router.push(`/milk-report?${queryStringify}`);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f4f6f9] h-full flex-1">
          <div
            className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2`}
          >
            <div className="relative overflow-x-auto">
              <div className="flex justify-between items-center mb-6">
                <div className="flex gap-4">
                  <div className="flex flex-col gap-2">
                    <label htmlFor="classId">Lớp</label>
                    <Select
                      id="classId"
                      size="small"
                      onChange={handleSelectClass}
                      defaultValue={query.classId || classes[0]?._id}
                    >
                      {classes?.map((item) => (
                        <MenuItem key={item._id} value={item._id}>
                          {item?.className}
                        </MenuItem>
                      ))}
                    </Select>
                  </div>
                  <div className="flex flex-col gap-2">
                    <label htmlFor="date">Ngày</label>
                    <DatePicker
                      defaultValue={dayjs(query.date)}
                      format="DD/MM/YYYY"
                      onChange={handleSelectDate}
                      slotProps={{ textField: { size: "small" } }}
                    />
                  </div>
                </div>
                <button
                  onClick={handleSave}
                  className="text-md px-2 py-3 font-medium border border-gray-200 hover:bg-gray-50 rounded uppercase disabled:opacity-50 disabled:cursor-not-allowed"
                  disabled={!isChange}
                >
                  <FontAwesomeIcon
                    className="text-blue-400 mr-2 text-lg"
                    icon={faFloppyDisk}
                  />
                  Lưu
                </button>
              </div>

              <div className="flex gap-10 mb-6">
                <span>Chú thích</span>
                <div>
                  <p>
                    Học sinh điểm danh{" "}
                    <span className="text-blue-400">Đi học</span> sẽ được chọn
                    sẵn là <span className="text-blue-400">Có uống</span>. Học
                    sinh điểm danh{" "}
                    <span className="text-red-400">
                      nghỉ học hoặc chưa điểm danh
                    </span>{" "}
                    sẽ chọn sẵn là{" "}
                    <span className="text-red-400">Không uống</span>
                  </p>
                  <p>
                    <span className="text-blue-400">Tổng kết tháng</span> được
                    hiển thị theo đúng tháng của ngày được chọn.
                  </p>
                  <p>
                    <span className="text-blue-400">Sửa báo uống</span> bằng cách
                    chọn ngày cần sửa, chọn trạng thái báo uống của học sinh và
                    bấm <span className="text-blue-400">LƯU</span>
                  </p>
                </div>
              </div>

              <table className="w-full text-md text-left rtl:text-right font-thin border">
                <thead className="bg-[#f5f6fa] border-b uppercase text-center">
                  <tr>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      STT
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Họ và tên
                    </th>
                    <th
                      scope="col"
                      colSpan={2}
                      className="px-6 py-3 font-thin border-r"
                    >
                      Tổng kết tháng {dayjs(query.date).format("MM/YYYY")}
                    </th>
                    <th
                      scope="col"
                      colSpan={2}
                      className="px-6 py-3 font-thin border-r text-center"
                    >
                      Báo uống ngày {dayjs(query.date).format("DD/MM/YYYY")}
                    </th>
                  </tr>
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 font-thin border-r"
                    ></th>
                    <th
                      scope="col"
                      className="px-6 py-3 font-thin border-r"
                    ></th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Đi học
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Có uống
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r ">
                      <p className="text-sm mb-4">Có uống</p>
                      <input
                        type="radio"
                        className="scale-125"
                        onChange={() => handleSelectAll(1)}
                        checked={selectAllValue === 1}
                      />
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r ">
                      <p className="text-sm mb-4">Không uống</p>
                      <input
                        type="radio"
                        className="accent-red-500 scale-125"
                        onChange={() => handleSelectAll(0)}
                        checked={selectAllValue === 0}
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {report?.map((item, index) => (
                    <tr key={item?._id} className="bg-white border-b">
                      <td className="px-6 py-4 border-r text-center">
                        {index + 1}
                      </td>
                      <td
                        scope="row"
                        className="px-6 py-4 whitespace-nowrap border-r"
                      >
                        {item?.fullName}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap border-r text-center">
                        {item?.numberOfMilkreports}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap border-r text-center">
                        {item?.numberOfMilks}
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap border-r text-center">
                        <input
                          value={1}
                          onChange={(e) => handleRadioChange(e, item)}
                          defaultChecked={item?.haveMilkOfSelectedDate}
                          checked={
                            item?.milkreportId === null ||
                            Number(map.get(item?.milkreportId))
                          }
                          type="radio"
                          className="scale-125"
                        />
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap border-r text-center">
                        <input
                          value={0}
                          onChange={(e) => handleRadioChange(e, item)}
                          defaultChecked={item?.haveMilkOfSelectedDate === 0}
                          checked={
                            Number(map.get(item?.milkreportId)) === 0 &&
                            item?.milkreportId !== null
                          }
                          type="radio"
                          className="accent-red-500 scale-125"
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
                <tfoot>
                  <tr>
                    <td
                      colspan={2}
                      className="px-6 py-4 bg-[#f5f6fa] border-b uppercase text-center"
                    >
                      Tổng kết
                    </td>
                    <td className="px-6 py-4 bg-[#f5f6fa] border-b uppercase text-center">
                      {totalNumMilkreport}
                    </td>
                    <td className="px-6 py-4 bg-[#f5f6fa] border-b uppercase text-center">
                      {totalNumMilks}
                    </td>
                    <td className="px-6 py-4 bg-[#f5f6fa] border-b uppercase text-center">
                      {totalHaveMilkOfSelectedDate}
                    </td>
                    <td className="px-6 py-4 bg-[#f5f6fa] border-b uppercase text-center">
                      {totalNotHaveMilkOfSelectedDate}
                    </td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </main>
      </div>
    </LocalizationProvider>
  );
}

export async function getServerSideProps({ query }) {
  const defaultQuery = {
    date: dayjs().format("YYYY-MM-DD"),
  };

  const queryStringify = stringifyUrl({ ...defaultQuery, ...query });
  const res = await axios.get(
    process.env.NEXT_PUBLIC_API_URL + `/milk-report?${queryStringify}`,
  );

  return {
    props: {
      report: res.data.data,
      totalNumMilkreport: res.data.totalNumMilkreport,
      totalNumMilks: res.data.totalNumMilks,
      totalHaveMilkOfSelectedDate: res.data.totalHaveMilkOfSelectedDate,
      totalNotHaveMilkOfSelectedDate: res.data.totalNotHaveMilkOfSelectedDate,
      query,
    },
  };
}
