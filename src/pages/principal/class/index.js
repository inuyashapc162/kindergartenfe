import Navbar from '@/components/Navbar';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
import Link from 'next/link';
import Image from 'next/image';
import edit from '../../../../public/img/icon-edit.png';
import tick from '../../../../public/img/tick.png';
import xtick from '../../../../public/img/unactive.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import decodeToken from "@/utils/auth";

const getYearFromDateString = (dateString) => {
  const date = new Date(dateString);
  return date.getUTCFullYear();
};

const Class = () => {
  const router = useRouter();
  const [listClass, setListClass] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');

  const getAllClass = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    const schoolID = (decode?.decoded?.data?.user?.schoolID);
    try {
      const result = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/classes/school/${schoolID}`,
        {
          headers: {
            "token": JSON.parse(token).token,
          },
        }
      );

      setListClass(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  const handleSearch = (e) => {
    e.preventDefault();
    const searchValue = e.target.search.value;
    setSearchTerm(searchValue);
  };

  const filteredList = listClass.filter(item =>
    item?.className.toLowerCase().includes(searchTerm.toLowerCase())
  );

  useEffect(() => {
    getAllClass();
  }, []);

  return (
    <>
      <Navbar />
      <div className='flex w-full'>
        <main className='p-2 bg-[#f0f0f0] h-full min-h-screen flex-1'>
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <form onSubmit={handleSearch} className='flex items-center px-2 border rounded'>
              <input
                type="text"
                className="p-2 rounded mr-2 focus:outline-none"
                placeholder="Type here..."
                name='search'
                id='search'
              />
              <button type='submit'>
                <FontAwesomeIcon className='cursor-pointer' icon={faMagnifyingGlass} />
              </button>
            </form>
            <button
              onClick={() => router.push("/principal/class/create")}
              className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded ml-auto"
            >
              Add class
            </button>

          </div>
          <div className='break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md'>
            <table className="w-full border-2 border-gray mx-auto bg-opacity-5">
              <thead className='bg-black bg-opacity-10'>
                <tr>
                  {/* <th className="border border-gray-300 p-2">ID</th> */}
                  <th className="border border-gray-300 p-2">Class Name</th>
                  <th className="border border-gray-300 p-2">End Year</th>
                  <th className="border border-gray-300 p-2">Active</th>
                  <th className="border border-gray-300 p-2">Edit</th>
                </tr>
              </thead>
              <tbody>
                {filteredList?.map((item) => (
                  <tr key={item?._id}>
                    {/* <td className="border border-gray-300 text-center">{item?._id}</td> */}
                    <td className="border border-gray-300 text-center">
                      {item?.className}
                    </td>
                    <td className="border border-gray-300 text-center">
                      {getYearFromDateString(item?.expiryDate)}
                    </td>
                    <td className="border border-gray-300 text-center">
                      <div className="flex items-center justify-center p-1">
                        {item?.isActive ? (
                          <Image width={18} height={18} src={tick} alt="active" />
                        ) : (
                          <Image width={18} height={18} src={xtick} alt="unactive" />
                        )}
                      </div>
                    </td>
                    <td className="border border-gray-300 text-center">
                      <div className="flex items-center justify-center p-1">
                        <Link href={`class/update/${item?._id}`}>
                          <Image width={18} height={18} src={edit} alt='Edit Image' />
                        </Link>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </main >
      </div>
    </>
  );
};

export default Class;
