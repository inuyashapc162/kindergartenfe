import Navbar from '@/components/Navbar';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';
import { toast } from 'react-toastify';
import decodeToken from "@/utils/auth";

const CreateClass = () => {
  const [className, setClassName] = useState('');
  const [schoolID, setSchoolID] = useState('');

  const getAccessToken = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    setSchoolID(decode?.decoded?.data?.user?.schoolID);
  };

  const router = useRouter();


  useEffect(() => {
    getAccessToken();
  }, []);

  const handleCreateClass = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/classes`, {
        className: className,
        schoolID: schoolID,
      });
      toast.success("Create class successfully!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      router.push('/principal/class');
      console.log('Class created successfully', response.data);
    } catch (error) {
      console.error('Error creating class:', error.message);
      toast.error("Create class failed!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
    }
  };
  return (
    <>
      <Navbar />
      <div className='flex w-full'>
        <main className='p-2 bg-[#f0f0f0] h-full min-h-screen flex-1'>
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <form onSubmit={handleCreateClass} className='w-full'>
              <div className='bg-white border p-5 shadow-md rounded'>
                <div className="max-w-2xl mx-auto p-4 flex flex-col md:flex-row md:space-x-4">
                  <div className="flex-grow">
                    <label htmlFor="class_name" className="block text-sm font-medium text-gray-700">
                      Class Name
                    </label>
                    <input
                      type="text"
                      id="class_name"
                      name="class_name"
                      className="mt-1 p-2 border rounded-md w-full"
                      value={className}
                      onChange={(e) => setClassName(e.target.value)}
                      placeholder='Enter class name'
                      required
                    />
                  </div>
                </div>
                <div className="max-w-2xl mx-auto p-4 flex flex-col">
                  <button type="submit" className="p-2 bg-blue-500 text-white rounded-md">
                    Create Class
                  </button>
                </div>
              </div>
            </form>
          </div>
        </main>
      </div>
    </>

  );
};

export default CreateClass;
