import Navbar from '@/components/Navbar';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';

//thêm trường teacher

const UpdateClass = () => {
  const [classData, setClassData] = useState({});
  const [className, setClassName] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [expiryDate, setExpiryDate] = useState('');
  const [initialExpiryDate, setInitialExpiryDate] = useState(null);
  const router = useRouter();
  const { id } = router.query;
  // console.log("🚀 ========= result:", id);

  const getClassData = async () => {
    try {
      const result = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/classes/${id}`);
      // console.log("🚀 ========= result:", result?.data);
      setClassData(result?.data?.data);
      setClassName(result?.data?.data?.className || '');
      setIsActive(result?.data?.data?.isActive || false);

      const formattedExpiryDate = result?.data?.data?.expiryDate
        ? new Date(result?.data?.data?.expiryDate).getUTCFullYear()
        : '';
      setExpiryDate(formattedExpiryDate);
      setInitialExpiryDate(formattedExpiryDate);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  useEffect(() => {
    getClassData();
  }, [id]);

  const updateClass = async (e) => {
    e.preventDefault();
    try {
      //If else để nếu không chỉnh sửa End Year không bị trả về năm 1970
      let updatedExpiryDate = expiryDate;
      if (expiryDate === null) {
        updatedExpiryDate = initialExpiryDate;
      } else {
        setInitialExpiryDate(expiryDate);
      }

      const updatedData = {
        className: className,
        isActive: isActive,
        expiryDate: updatedExpiryDate,
      };

      await axios.put(`${process.env.NEXT_PUBLIC_API_URL}/classes/${id}`, updatedData);
      console.log('Class updated successfully!');
      toast.success("Update class successfully!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      router.push('/principal/class');
    } catch (error) {
      console.error('Error updating class:', error);
      toast.error("Update class failed!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
    }
    getClassData();
  };

  return (
    <>
      <Navbar />
      <div className='flex w-full'>
        <main className='p-2 bg-[#f0f0f0] h-full min-h-screen flex-1'>
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <form onSubmit={updateClass} className='w-full'>
              <div className='bg-white border p-5 shadow-md rounded'>
                <div className="flex-grow mb-4">
                  <label htmlFor="class_name" className="block text-sm font-medium text-gray-700">
                    Class Name
                  </label>
                  <input
                    type="text"
                    id="class_name"
                    name="class_name"
                    className="mt-1 p-2 border rounded-md w-full"
                    defaultValue={className}
                    onChange={(e) => setClassName(e.target.value)}
                    required
                  />
                </div>
                <div className="flex-grow mb-4">
                  <label htmlFor="expired_date" className="block text-sm font-medium text-gray-700">
                    End Date
                  </label>
                  <input
                    type="number"
                    id="expired_date"
                    name="expired_date"
                    className="mt-1 p-2 border rounded-md w-full"
                    defaultValue={expiryDate === null ? '' : expiryDate}
                    onChange={(e) => setExpiryDate(e.target.value)}
                    required
                  />
                </div>
                <div className="flex-grow mb-4">
                  <label htmlFor="active" className="block text-sm font-medium text-gray-700">
                    Class Status
                  </label>
                  <select
                    id="active"
                    name="active"
                    className="mt-1 p-2 border rounded-md w-full"
                    value={isActive ? 'active' : 'inactive'}
                    onChange={(e) => setIsActive(e.target.value === 'active')}
                  >
                    <option value="active">Active</option>
                    <option value="inactive">Inactive</option>
                  </select>
                </div>
                <div className="m-auto mt-4 p-4 flex justify-center">
                  <button
                    type="submit"
                    className="px-4 py-2 bg-blue-500 text-white rounded-md"
                  >
                    Update Class
                  </button>
                </div>
              </div>

            </form>
          </div>
        </main>
      </div>

    </>
  );
};

export default UpdateClass;
