import Navbar from "@/components/Navbar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPenToSquare, faSort } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import axios from "axios";
import { useForm } from "react-hook-form";
import { PHONE_NUMBER_REGEX } from "@/constants/regex";
import { useRouter } from "next/router";
import { useState } from "react";
import stringifyUrl from "@/utils/stringifyUrl";
import qs from "query-string";
const DEFAULT_LIMIT = 20;

export default function Page({
  teachers,
  currentPage,
  totalPages,
  totalCount,
  query,
}) {
  const [sortIndex, setSortIndex] = useState(query?.sortIndex || -1);
  const [sortLastName, setSortLastName] = useState(query?.sortLastName || -1);

  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const generatePageLink = (page) => {
    const queryStringify = stringifyUrl({ ...query, page });
    return `/principal/teacher?${queryStringify}`;
  };

  const generateSortIndex = () => {
    setSortIndex(sortIndex * -1);
    const queryStringify = stringifyUrl({
      ...query,
      sortIndex,
    });

    router.push(`/principal/teacher?${queryStringify}`);
  };

  const generateSortLastName = () => {
    setSortLastName(sortLastName * -1);
    const queryStringify = stringifyUrl({ ...query, sortLastName });
    router.push(`/principal/teacher?${queryStringify}`);
  };

  const onSubmit = (data) => {
    const query = qs.stringify(data);
    router.push(`/principal/teacher?${query}`);
  };
  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#ffff] h-full flex-1">
          {/* Teacher List Header */}
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between">
            <form
              action={onSubmit}
              onSubmit={handleSubmit(onSubmit)}
              className="flex gap-2"
            >
              <div className="flex flex-col gap-2">
                <label htmlFor="lastName">Tên</label>
                <input
                  {...register("lastName")}
                  defaultValue={query?.lastName || ""}
                  id="lastName"
                  type="text"
                  className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  placeholder="Nguyen Van A..."
                />
              </div>
              <div className="flex flex-col gap-2">
                <label htmlFor="phoneNumber">Số điện thoại</label>
                <input
                  defaultValue={query?.phoneNumber || ""}
                  {...register("phoneNumber", {
                    pattern: {
                      value: PHONE_NUMBER_REGEX,
                      message: "Định dạng số điện thoại không hợp lệ",
                    },
                  })}
                  id="phoneNumber"
                  className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  placeholder="0969218000..."
                />
                {errors.phoneNumber && (
                  <span className="text-rose-500 text-sm">
                    {errors.phoneNumber.message}
                  </span>
                )}
              </div>
              <input
                {...register("page")}
                hidden
                defaultValue={query?.page || 1}
              />
              <input type="submit" hidden />
            </form>
            {/* <Link href="/principal/teacher/create">
              <button className="text-sm px-2 py-3 font-medium text-white bg-[#2BB56C] border border-[#2BB56C] hover:bg-[#00B153] rounded uppercase">
                Thêm mới
              </button>
            </Link> */}
          </div>

          {/* Teacher List Table */}
          <div
            className={`break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2`}
          >
            <div className="relative overflow-x-auto">
              <table className="w-full text-md text-left rtl:text-right font-thin border">
                <thead className="bg-[#f5f6fa] border-b">
                  <tr>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      STT{" "}
                      <FontAwesomeIcon
                        onClick={generateSortIndex}
                        className="hover:text-gray-500"
                        icon={faSort}
                      />
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Họ
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Tên{" "}
                      <FontAwesomeIcon
                        onClick={generateSortLastName}
                        className="hover:text-gray-500"
                        icon={faSort}
                      />
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Địa chỉ
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Số điện thoại
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Email
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Trường
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin border-r">
                      Lớp
                    </th>
                    <th scope="col" className="px-6 py-3 font-thin text-center">
                      Thông tin chi tiết
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {teachers?.map((teacher, index) => (
                    <tr key={teacher?._id} className="bg-white border-b">
                      <td className="px-6 py-4 border-r">
                        {index + 1 + DEFAULT_LIMIT * (currentPage - 1)}
                      </td>
                      <td
                        scope="row"
                        className="px-6 py-4 whitespace-nowrap border-r"
                      >
                        {teacher?.firstName}
                      </td>
                      <td className="px-6 py-4 border-r">
                        {teacher?.lastName}
                      </td>
                      <td className="px-6 py-4 border-r">{teacher?.address}</td>
                      <td className="px-6 py-4 border-r">
                        {teacher?.phoneNumber}
                      </td>
                      <td className="px-6 py-4 border-r">{teacher?.email}</td>
                      <td className="px-6 py-4 border-r">
                        <p key={teacher?.schoolID?._id}>
                          {teacher?.schoolID?.schoolName}
                        </p>
                      </td>
                      <td className="px-6 py-4 border-r">
                        <p key={teacher?.classID?._id}>
                          {teacher?.classID?.className}
                        </p>
                      </td>
                      <td className="text-center">
                        <Link
                          href={`/principal/teacher/update/${teacher?._id}`}
                        >
                          <FontAwesomeIcon
                            icon={faPenToSquare}
                            className="text-sky-600 cursor-pointer hover:text-sky-800"
                          />
                        </Link>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <p className="font-thin mt-3">
              Hiển thị {1 + DEFAULT_LIMIT * (currentPage - 1)} -{" "}
              {teachers?.length + DEFAULT_LIMIT * (currentPage - 1)} trong tổng
              số {totalCount} Giáo viên
            </p>

            {/* Table Pagination*/}
            <nav aria-label="Page navigation example">
              <div className="flex justify-end items-center -space-x-px h-8 text-sm mt-2">
                <button>
                  <Link
                    href={
                      currentPage === 1
                        ? generatePageLink(currentPage)
                        : generatePageLink(currentPage - 1)
                    }
                    className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-e-0 border-gray-300 rounded-s hover:bg-gray-100 hover:text-gray-700"
                  >
                    <span className="sr-only">Previous</span>
                    <svg
                      className="w-2.5 h-2.5 rtl:rotate-180"
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 6 10"
                    >
                      <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M5 1 1 5l4 4"
                      />
                    </svg>
                  </Link>
                </button>
                {Array.from({ length: totalPages }, (_, i) => i + 1).map(
                  (page) => (
                    <button key={page}>
                      <Link
                        href={generatePageLink(page)}
                        className={`flex items-center justify-center px-3 h-8 leading-tight border border-gray-300 hover:bg-gray-100 hover:text-gray-700 ${
                          currentPage === page
                            ? "bg-blue-500 text-white"
                            : "bg-white text-gray-500"
                        }`}
                      >
                        {page}
                      </Link>
                    </button>
                  ),
                )}
                <button>
                  <Link
                    href={
                      currentPage === totalPages
                        ? generatePageLink(currentPage)
                        : generatePageLink(currentPage + 1)
                    }
                    className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e hover:bg-gray-100 hover:text-gray-700"
                  >
                    <span className="sr-only">Next</span>
                    <svg
                      className="w-2.5 h-2.5 rtl:rotate-180"
                      aria-hidden="true"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 6 10"
                    >
                      <path
                        stroke="currentColor"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="m1 9 4-4-4-4"
                      />
                    </svg>
                  </Link>
                </button>
              </div>
            </nav>
          </div>
        </main>
      </div>
    </>
  );
}

export async function getServerSideProps({ query }) {
  const queryStringify = stringifyUrl(query);
  const res = await axios.get(
    process.env.NEXT_PUBLIC_API_URL + `/teachers?${queryStringify}`,
  );

  console.log(res.data.totalCount);

  return {
    props: {
      teachers: res.data.data,
      currentPage: res.data.currentPage,
      totalPages: res.data.totalPages,
      totalCount: res.data.totalCount,
      query,
    },
  };
}
