import Navbar from "@/components/Navbar";
import Sidebar from "@/components/Sidebar";
import { EMAIL_REGEX, PHONE_NUMBER_REGEX } from "@/constants/regex";
import { MenuItem, Select } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-toastify";

export default function Page() {
  const [schools, setSchools] = useState();
  const [classes, setClasses] = useState();

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    control,
  } = useForm();

  const fetchSchools = async () => {
    const res = await axios.get(process.env.NEXT_PUBLIC_API_URL + `/schools`, {
      headers: {
        token: `${JSON.parse(localStorage.getItem("token"))?.token} `,
      },
    });
    setSchools(res?.data?.data);
  };

  const fetchClasses = async () => {
    const res = await axios.get(process.env.NEXT_PUBLIC_API_URL + `/classes`, {
      headers: {
        token: `${JSON.parse(localStorage.getItem("token"))?.token} `,
      },
    });
    setClasses(res?.data?.data);
  };

  useEffect(() => {
    fetchSchools();
    fetchClasses();
  }, []);

  const onSubmit = async (data) => {
    try {
      const res = await axios.post(
        process.env.NEXT_PUBLIC_API_URL + `/teachers`,
        {
          ...data,
          schoolID: data?.schoolID,
          classID: data?.classID,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")} `,
          },
        },
      );

      if (res) {
        toast.success("Tạo mới giáo viên thành công");
        reset();
      }
    } catch (error) {
      toast.error("Taọ mới giáo viên thất bại");
    }
  };

  return (
    <>
      <Navbar />
      <div className="flex w-full">
        {/* <Sidebar /> */}
        <main className="p-2 bg-[#f4f6f9] h-full font-thin flex-1">
          <div
            className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2 `}
          >
            <p className="font-thin text-xl mb-3">Thêm mới giáo viên</p>
            <hr className="mb-4" />
            <form
              onSubmit={handleSubmit(onSubmit)}
              className="flex flex-col gap-4"
            >
              <p className="text-[#0497D8] uppercase">1. Thông tin giáo viên</p>
              <div className="flex gap-4">
                <div className="flex flex-col gap-2 flex-1">
                  <label htmlFor="firstName">
                    Họ <span className="text-rose-500">(*)</span>
                  </label>
                  <input
                    id="firstName"
                    {...register("firstName", { required: true })}
                    className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  />
                  {errors.firstName && (
                    <span className="text-rose-500 text-sm">
                      Truờng này là bắt buộc
                    </span>
                  )}
                </div>
                <div className="flex flex-col gap-2 flex-1">
                  <label htmlFor="lastName">
                    Tên <span className="text-rose-500">(*)</span>
                  </label>
                  <input
                    id="lastName"
                    {...register("lastName", { required: true })}
                    className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  />
                  {errors.lastName && (
                    <span className="text-rose-500 text-sm">
                      Truờng này là bắt buộc
                    </span>
                  )}
                </div>
              </div>

              <div className="flex flex-col gap-2">
                <label htmlFor="address">Địa chỉ</label>
                <input
                  id="address"
                  {...register("address")}
                  className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                />
              </div>

              <div className="flex gap-4">
                <div className="flex flex-col gap-2 flex-1">
                  <label htmlFor="phoneNumber">Số điện thoại</label>
                  <input
                    id="phoneNumber"
                    {...register("phoneNumber", {
                      pattern: {
                        value: PHONE_NUMBER_REGEX,
                        message: "Định dạng số điện thoại không hợp lệ",
                      },
                    })}
                    className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  />
                  {errors.phoneNumber && (
                    <span className="text-rose-500 text-sm">
                      {errors.phoneNumber.message}
                    </span>
                  )}
                </div>

                <div className="flex flex-col gap-2 flex-1">
                  <label htmlFor="email">Email</label>
                  <input
                    id="email"
                    {...register("email", {
                      pattern: {
                        value: EMAIL_REGEX,
                        message: "Định dạng email không hợp lệ",
                      },
                    })}
                    className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  />
                  {errors.email && (
                    <span className="text-rose-500 text-sm">
                      {errors.email.message}
                    </span>
                  )}
                </div>
              </div>

              <p className="text-[#0497D8] uppercase">
                2. Thông tin trường học
              </p>
              <div className="flex flex-col gap-2 flex-1">
                <label htmlFor="schoolID">Trường</label>
                <Controller
                  control={control}
                  name="schoolID"
                  render={({ field: { onChange, onBlur, value } }) => (
                    <Select
                      id="schoolID"
                      size="small"
                      value={value}
                      onBlur={onBlur}
                      onChange={onChange}
                    >
                      {schools?.map((item) => (
                        <MenuItem key={item._id} value={item._id}>
                          {item?.schoolName}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                />
              </div>
              <p className="text-[#0497D8] uppercase">3. Thông tin lớp học</p>
              <div className="flex flex-col gap-2 flex-1">
                <label htmlFor="classID">Lớp</label>
                <Controller
                  control={control}
                  name="classID"
                  render={({ field: { onChange, onBlur, value } }) => (
                    <Select
                      id="classID"
                      size="small"
                      onChange={onChange}
                      onBlur={onBlur}
                      value={value}
                    >
                      {classes?.map((item) => (
                        <MenuItem key={item._id} value={item._id}>
                          {item?.className}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                />
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
                >
                  Lưu
                </button>
              </div>
            </form>
          </div>
        </main>
      </div>
    </>
  );
}
