import * as XLSX from "xlsx";
import axios from "axios";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import React, { useEffect, useState } from "react";
import Navbar from "@/components/Navbar";
import { Controller, useForm } from "react-hook-form";
import { DAYCARE_CENTER, KINDERGARTEN } from '../../constants/grade.js';
import { useRouter } from 'next/router';
import moment from 'moment';
import decodeToken from "@/utils/auth";
import Link from "next/link.js";
function Apps() {

  const router = useRouter();
  const [data, setData] = useState([]);
  const { register, handleSubmit, setValue, control } = useForm();
  const weekList = createWeekList();
  const [user, setUser] = useState([]);
  const [gradeBox, setGradeBox] = useState(DAYCARE_CENTER);
  const [weekBox, setWeekBox] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const token = localStorage.getItem("token");
      if (token) {
        try {
          const decode = await decodeToken(JSON.parse(token)?.token);
          console.log("decode", decode);
          setUser(decode?.decoded?.data?.user);
        } catch (error) {
          console.error("Error decoding token:", error);
        }
      }
    };
    fetchData();
  }, []);


  useEffect(() => {
    const currentDate = moment();
    let foundCurrentWeek = false;
    weekList.forEach((w) => {
      const startWeek = moment(w.startWeek).startOf('day');
      const endWeek = moment(w.endWeek).endOf('day');
      if (currentDate.isBetween(startWeek, endWeek)) {
        setWeekBox(w.weekNo);
        foundCurrentWeek = true;
      }
    });
    if (!foundCurrentWeek) {
      setWeekBox(weekList[0].weekNo);
    }
  }, []);

  const handleGradeChange = (event) => {
    const grade = event.target.value;
    setGradeBox(grade);
  };

  const handleWeekChange = (event) => {
    const week = event.target.value;
    setWeekBox(week);
  };


  function createWeekList() {
    const currentDate = moment(); // Get the current date from the device
    const september5th = moment(currentDate.format('YYYY-09-05')); // September 5th of the current year
    const currentYear = currentDate.year(); // Get the current year

    let startDate;
    if (currentDate.isBefore(september5th)) {
      startDate = moment(`${currentYear - 1}-09-05`);
    } else {

      startDate = moment(`${currentYear}-09-05`);
    }
    const numberOfWeeks = 35;
    const weekList = [];
    for (let i = 0; i < numberOfWeeks; i++) {
      const endDate = startDate.clone().add(6, 'days');
      const week = {
        weekNo: i + 1,
        startWeek: startDate.clone(),
        endWeek: endDate
      };
      weekList.push(week);
      startDate.add(7, 'days');
    }
    return weekList;
  }

  useEffect(() => {
    const fetchData = async () => {
      // console.log(gradeBox, weekBox);
      // console.log(user);
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/menus`, {
          params: { schoolId: user?.schoolID, grade: gradeBox, noWeek: weekBox }
        });
        // console.log("responese", response?.data?.data);
        handleFileUpload(response?.data?.data);
      } catch (error) {
        console.log("Error:", error);
      }
    };

    fetchData();
  }, [gradeBox, weekBox, user]);

  const handleFileUpload = (fileData) => {
    const workbook = XLSX.read(fileData, { type: "base64" });
    const sheetName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[sheetName];
    const parsedData = XLSX.utils.sheet_to_json(sheet, { defval: null });
    console.log(parsedData, "menu");
    setData(parsedData);
  };

  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f4f6f9] h-full font-thin flex-1 h-min-screen">
          <div
            className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2 `}
          >
            <form className="mt-8 space-y-3" >
              <div className="grid grid-cols-1 space-y-2">
                {
                  user?.role?.roleName === "Principal" && (
                    <Link href="/menu/create"
                      className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
                    >
                      Add/Update Menu
                    </Link>
                  )
                }
                <div className=" grid grid-cols-4 gap-2 flex-1">
                  <div className="col-span-1">
                    <div className="flex flex-col">
                      <label htmlFor="lastName" className="font-bold">
                        Grade
                      </label>
                      <select
                        {...register("grade", { required: true })}
                        className="p-2 border rounded-md"
                        onChange={handleGradeChange}
                      >
                        <option value="" disabled>Select grade</option>
                        <option value={DAYCARE_CENTER}>Ages Under 3</option>
                        <option value={KINDERGARTEN}>Ages 3 to 6</option>
                      </select>
                    </div>
                  </div>

                  <div className="col-span-1">
                    <div className="flex flex-col">
                      <label className="font-bold">
                        Week
                      </label>

                      <select
                        {...register("weekNo", { required: true })}
                        className="p-2 border rounded-md"
                        onChange={handleWeekChange}
                        value={weekBox}
                      >
                        {
                          weekList?.map((week => (
                            <option key={week.weekNo} value={week.weekNo}>Week {week.weekNo}:{'\u00A0'}
                              {week.startWeek.format('DD/MM/YYYY')} to {week.endWeek.format('DD/MM/YYYY')}</option>
                          ))
                          )
                        }
                      </select>
                    </div>
                  </div>
                </div>


                <div  >
                  {data.length === 0 ? (
                    <p>Không có dữ liệu</p>
                  ) : (
                    <div className="mt-6">
                      <table className="min-w-full" aria-label="simple table">
                        <thead >
                          <tr className="grid grid-cols-7 bg-gray-100 ">
                            {Object.keys(data[0]).map((key) => (
                              <th key={key} className="py-2 px-4 border col-span-1 font-bold text-blue-400">{key}</th>
                            ))}
                          </tr>
                        </thead>
                        <tbody>
                          {data.map((row, index) => (
                            <tr key={index} className="grid grid-cols-7 ">
                              {Object.values(row).map((value, index) => (
                                index === 0 ? (
                                  <td key={index} className="py-2 px-4  border col-span-1">{value}</td>
                                ) : (<td key={index} className="py-2 px-4 border col-span-1">{value}</td>)
                              ))}
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  )}
                </div>

              </div>
            </form>
          </div>
        </main>
      </div>


    </>

  );
}

export default Apps;



