import Navbar from "@/components/Navbar";
import decodeToken from "@/utils/auth";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { DAYCARE_CENTER, KINDERGARTEN } from '../../../constants/grade.js';
import { useForm } from "react-hook-form";
import moment from 'moment';
import Link from "next/link.js";
import { read, utils } from 'xlsx';


export default function FileUpload() {
  const router = useRouter();
  const { register, handleSubmit, setValue, control } = useForm();
  const [selectedFile, setSelectedFile] = useState(null);
  const [fileName, setFileName] = useState(null);
  const [user, setUser] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const token = localStorage.getItem("token");
      if (token) {
        try {
          const decode = await decodeToken(JSON.parse(token)?.token);
          console.log("decode", decode);
          setUser(decode?.decoded?.data?.user);
        } catch (error) {
          console.error("Error decoding token:", error);
        }
      }
    };
    fetchData();
  }, []);

  const handleFileChange = async (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);
    setFileName(file.name);
  };

  const onSubmit = async (data) => {
    // event.preventDefault();
    if (!selectedFile) {
      alert("Please select a file");
      return;
    }
    const formData = new FormData();
    formData.append("fileMenu", selectedFile);
    formData.append("userId", user?._id);
    formData.append("schoolID", user?.schoolID);
    formData.append("noWeek", "0");
    formData.append("grade", "0");
    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_API_URL}/menus`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      toast.success("Sample update successfully", {
        position: "top-right",
        autoClose: 3000, // Đóng sau 3 giây               
      });
      router.push("/menu");
      console.log("🚀 ========= response:", response);
    } catch (error) {
      console.error("Error uploading sample file:", error);
    }
  };

  return (
    <div className="flex w-full">
      {/* <Sidebar /> */}
      <main className="p-2 bg-[#f4f6f9] h-full font-thin flex-1">
        <div className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2 `}>
          <Navbar />
          <form className="mt-8 space-y-3" onSubmit={handleSubmit(onSubmit)}>
            <div className="grid grid-cols-1 space-y-2">
              <label className="text-sm font-bold text-gray-500 tracking-wide">
                UPLOAD SAMPLE MENU FILE
              </label>
              <div className="flex items-center justify-center w-full">
                <div className="grid grid-cols-2 gap-2 flex-1">
                  <div className="col-span-2">
                    <label className="flex flex-col rounded-lg border-4 border-dashed w-full h-60 p-10 group text-center">
                      <div className="h-full w-full text-center flex flex-col items-center justify-center items-center">
                        {fileName ? (
                          <span>{fileName}</span>
                        ) : (
                          <>
                            <div className="flex flex-auto max-h-48 w-2/5 mx-auto -mt-10 justify-center">
                              <img
                                className="has-mask h-36 object-center"
                                src="https://img.freepik.com/free-vector/image-upload-concept-landing-page_52683-27130.jpg?size=338&ext=jpg"
                                alt="freepik image"
                              />
                            </div>
                            <p className="pointer-none text-gray-500 ">
                              Upload Menu Sample Excel File
                            </p>
                          </>
                        )}
                      </div>
                      <input
                        type="file"
                        id="fileInput"
                        className="hidden"
                        accept=".xlsx, .xls"
                        onChange={handleFileChange}
                      />
                    </label>
                  </div>
                </div>

                <div className="col-span-1">
                </div>
              </div>
            </div>
            <div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="my-5 w-60 flex justify-center bg-blue-500 text-gray-100 p-3 rounded-full tracking-wide
                              font-semibold  focus:outline-none focus:shadow-outline hover:bg-blue-600 shadow-lg cursor-pointer transition ease-in duration-300"
                >
                  Upload
                </button>
              </div>

            </div>
          </form>
        </div>
      </main>
    </div>



  );
}