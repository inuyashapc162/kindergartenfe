import Navbar from "@/components/Navbar";
import decodeToken from "@/utils/auth";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { DAYCARE_CENTER, KINDERGARTEN } from '../../../constants/grade.js';
import { useForm } from "react-hook-form";
import moment from 'moment';
import Link from "next/link.js";
import { read, utils } from 'xlsx';
import * as XLSX from "xlsx";

export default function FileUpload() {
  const router = useRouter();
  const { register, handleSubmit, setValue, control } = useForm();
  const [selectedFile, setSelectedFile] = useState(null);
  const [fileName, setFileName] = useState(null);
  const [user, setUser] = useState();
  const [sampleFile, setSampleFile] = useState(null);
  useEffect(() => {
    const fetchData = async () => {
      const token = localStorage.getItem("token");
      if (token) {
        try {
          const decode = await decodeToken(JSON.parse(token)?.token);
          console.log("decode", decode);
          setUser(decode?.decoded?.data?.user);
        } catch (error) {
          console.error("Error decoding token:", error);
        }
      }
    };
    fetchData();
  }, []);


  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/menus`, {
          params: { schoolId: user?.schoolID, grade: 0, noWeek: 0 }
        });
        // console.log("responese", response?.data?.data);
        handleFileUpload(response?.data?.data);
      } catch (error) {
        console.log("Error:", error);
      }
    };
    fetchData();
  }, [user]);


  const handleFileUpload = (fileData) => {
    const workbook = XLSX.read(fileData, { type: "base64" });
    const sheetName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[sheetName];
    const parsedData = XLSX.utils.sheet_to_json(sheet, { defval: null });
    console.log(parsedData, "menu");
    setSampleFile(parsedData);
  };

  const handleFileChange = async (event) => {
    const row = Object.values(sampleFile)[0];
    const colA1 = Object.keys(row)[0];
    const colA2 = Object.keys(row)[1];
    console.log(colA1, colA2, typeof colA1);
    const file = event.target.files[0];
    try {
      const reader = new FileReader();
      reader.onload = (e) => {
        const data = new Uint8Array(e.target.result);
        const workbook = read(data, { type: 'array' });
        const worksheet = workbook.Sheets[workbook.SheetNames[0]];
        const cellValue = worksheet['A1'].v;
        const cellValue1 = worksheet['B1'].v;
        if (cellValue != colA1 && cellValue1 != colA2) {
          toast("INVALID EXCEL FILE");
        } else {
          setSelectedFile(file);
          setFileName(file.name);
        }
      };
      reader.readAsArrayBuffer(file);
    } catch (error) {
      console.error(error);
    }
  };


  function createWeekList() {
    const currentDate = moment(); // Get the current date from the device
    const september5th = moment(currentDate.format('YYYY-09-05')); // September 5th of the current year
    const currentYear = currentDate.year(); // Get the current year

    let startDate;
    if (currentDate.isBefore(september5th)) {
      // If the current date is before September 5th, subtract 1 year from the current year
      startDate = moment(`${currentYear - 1}-09-05`);
    } else {
      // Otherwise, use the current year
      startDate = moment(`${currentYear}-09-05`);
    }
    const numberOfWeeks = 35; // Number of weeks
    const weekList = [];

    for (let i = 0; i < numberOfWeeks; i++) {
      const endDate = startDate.clone().add(6, 'days'); // Calculate the end date (start date + 6 days)

      const week = {
        weekNo: i + 1,
        weekRange: `${startDate.format('DD/MM/YYYY')} to ${endDate.format('DD/MM/YYYY')}`
      };
      weekList.push(week);
      startDate.add(7, 'days'); // Update the start date for the next week
    }
    return weekList;
  }

  const weekList = createWeekList();

  const onSubmit = async (data) => {
    // event.preventDefault();
    if (!selectedFile) {
      alert("Please select a file");
      return;
    }
    const formData = new FormData();
    formData.append("fileMenu", selectedFile);
    formData.append("userId", user?._id);
    formData.append("schoolID", user?.schoolID);
    formData.append("noWeek", data?.weekNo);
    formData.append("grade", data?.grade);
    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_API_URL}/menus`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      toast.success("File uploaded successfully", {
        position: "top-right",
        autoClose: 3000, // Đóng sau 3 giây
      });
      router.push("/menu");
      console.log("🚀 ========= response:", response);
      console.log("File uploaded successfully");
    } catch (error) {
      console.error("Error uploading file:", error);
    }
  };

  return (
    <div className="flex w-full">
      {/* <Sidebar /> */}
      <main className="p-2 bg-[#f4f6f9] h-full font-thin flex-1">
        <div className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2 `}>
          <Navbar />
          <form className="mt-8 space-y-3" onSubmit={handleSubmit(onSubmit)}>
            <div className="grid grid-cols-1 space-y-2">
              <div className="grid grid-cols-4 gap-2 flex-1">
                <div className="col-span-1">
                  <div className="flex flex-col">
                    <label htmlFor="lastName">
                      Grade <span className="text-rose-500">(*)</span>
                    </label>
                    <select
                      {...register("grade", { required: true })}
                      className="p-2 border rounded-md"
                    >
                      <option value="" disabled>Select grade</option>
                      <option value={DAYCARE_CENTER}>Ages Under 3</option>
                      <option value={KINDERGARTEN}>Ages 3 to 6</option>
                    </select>
                  </div>
                </div>

                <div className="col-span-1">
                  <div className="flex flex-col">
                    <label htmlFor="lastName">
                      Week <span className="text-rose-500">(*)</span>
                    </label>
                    <select
                      {...register("weekNo", { required: true })}
                      className="p-2 border rounded-md"
                    >
                      {
                        weekList?.map((week => (
                          <option key={week.weekNo} value={week.weekNo} >Week {week.weekNo}: {week.weekRange}</option>
                        )))}
                    </select>
                  </div>
                </div>
              </div>
              <Link href="/menu/updateSample"
                className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-green-500 text-white font-bold py-2 px-4 rounded"
              >
                Update Sample File
              </Link>
              <label className="text-sm font-bold text-gray-500 tracking-wide">
                UPLOAD FILE EXCEL
              </label>
              <div className="flex items-center justify-center w-full">
                <div className="grid grid-cols-2 gap-2 flex-1">
                  <div className="col-span-1">
                    <label className="flex flex-col rounded-lg border-4 border-dashed w-full h-60 p-10 group text-center">

                      {/* <Link href={`${process.env.NEXT_PUBLIC_API_URL}/files/${item?._id}`}> */}
                      <Link href={`${process.env.NEXT_PUBLIC_API_URL}/menus/${user?.schoolID}`}>
                        <div className="h-full w-full text-center flex flex-col items-center justify-center items-center">
                          {(
                            <>
                              <div className="flex flex-auto max-h-48 w-2/5 mx-auto -mt-10 justify-center">
                                <img
                                  className="has-mask h-36 object-center"
                                  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlpFzhsEnEbGHvWhWnyIVOPJeQlMi5URKPEwOGKJtoKiGOip4mbnGLz2KrhDXWUHue5rs&usqp=CAU"
                                  alt="freepik image"
                                />
                              </div>
                              <p className="pointer-none text-gray-500 ">
                                Download Sample Excel File
                              </p>
                            </>
                          )}
                        </div>
                      </Link>
                    </label>
                  </div>
                  <div className="col-span-1">
                    <label className="flex flex-col rounded-lg border-4 border-dashed w-full h-60 p-10 group text-center">
                      <div className="h-full w-full text-center flex flex-col items-center justify-center items-center">
                        {fileName ? (
                          <span>{fileName}</span>
                        ) : (
                          <>
                            <div className="flex flex-auto max-h-48 w-2/5 mx-auto -mt-10 justify-center">
                              <img
                                className="has-mask h-36 object-center"
                                src="https://img.freepik.com/free-vector/image-upload-concept-landing-page_52683-27130.jpg?size=338&ext=jpg"
                                alt="freepik image"
                              />
                            </div>
                            <p className="pointer-none text-gray-500 ">
                              Upload Menu Exel File
                            </p>
                          </>
                        )}
                      </div>
                      <input
                        type="file"
                        id="fileInput"
                        className="hidden"
                        accept=".xlsx, .xls"
                        onChange={handleFileChange}
                      />
                    </label>
                  </div>
                </div>

              </div>
            </div>
            <div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="my-5 w-60 flex justify-center bg-blue-500 text-gray-100 p-3 rounded-full tracking-wide
                              font-semibold  focus:outline-none focus:shadow-outline hover:bg-blue-600 shadow-lg cursor-pointer transition ease-in duration-300"
                >
                  Upload
                </button>
              </div>

            </div>
          </form>
        </div>
      </main>
    </div>



  );
}