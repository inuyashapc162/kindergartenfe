import Navbar from "@/components/Navbar";
import decodeToken from "@/utils/auth";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";

export default function FileUpload() {
  const router = useRouter();

  const [selectedFile, setSelectedFile] = useState(null);
  const [fileName, setFileName] = useState(null);
  const [token, setToken] = useState();
  const handleFileChange = (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);
    setFileName(file.name);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!selectedFile) {
      alert("Please select a file");
      return;
    }

    const formData = new FormData();
    formData.append("file", selectedFile);
    formData.append("userId", token?.user?._id);
    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_API_URL}/files`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      toast.success("File uploaded successfully", {
        position: "top-right",
        autoClose: 3000, // Đóng sau 3 giây
      });
      router.push("/curriculum/plan");
      console.log("🚀 ========= response:", response);
      console.log("File uploaded successfully");
    } catch (error) {
      console.error("Error uploading file:", error);
    }
  };
  const getAccessToken = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    setToken(decode?.decoded?.data);
  };

  useEffect(() => {
    getAccessToken();
  }, []);
  return (
    <div className="bg-[#ffffff] h-full min-h-screen flex-1">
      <Navbar />
      <form className="mt-8 space-y-3 px-20" onSubmit={handleSubmit}>
        <div className="grid grid-cols-1 space-y-2">
          <label className="text-sm font-bold text-gray-500 tracking-wide">
            Attach Document
          </label>
          <div className="flex items-center justify-center w-full">
            <label className="flex flex-col rounded-lg border-4 border-dashed w-full h-60 p-10 group text-center">
              <div className="h-full w-full text-center flex flex-col items-center justify-center items-center">
                {fileName ? (
                  <span>{fileName}</span>
                ) : (
                  <>
                    <div className="flex flex-auto max-h-48 w-2/5 mx-auto -mt-10 justify-center">
                      <img
                        className="has-mask h-36 object-center"
                        src="https://img.freepik.com/free-vector/image-upload-concept-landing-page_52683-27130.jpg?size=338&ext=jpg"
                        alt="freepik image"
                      />
                    </div>
                    <p className="pointer-none text-gray-500 ">
                      <span className="text-sm">Drag and drop</span> files here{" "}
                      <br /> or{" "}
                      <label
                        htmlFor="fileInput"
                        className="text-blue-600 hover:underline cursor-pointer"
                      >
                        select a file
                      </label>{" "}
                      from your computer
                    </p>
                  </>
                )}
              </div>
              <input
                type="file"
                id="fileInput"
                className="hidden"
                onChange={handleFileChange}
              />
            </label>
          </div>
        </div>
        <p className="text-sm text-gray-300">
          <span>File type: doc, pdf, types of images</span>
        </p>
        <div>
          <button
            type="submit"
            className="my-5 w-full flex justify-center bg-blue-500 text-gray-100 p-4 rounded-full tracking-wide
                              font-semibold  focus:outline-none focus:shadow-outline hover:bg-blue-600 shadow-lg cursor-pointer transition ease-in duration-300"
          >
            Upload
          </button>
        </div>
      </form>
    </div>
  );
}
