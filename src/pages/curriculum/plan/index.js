import React, { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Link from "next/link";
import { Button } from "@mui/material";
import Navbar from "../../../components/Navbar";
import dayjs from "dayjs";
export default function ListAccount() {
  const [data, setData] = useState();
  const router = useRouter();
  const getAllPlan = async () => {
    try {
      const result = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/files`);
      console.log("🚀 ========= result:", result);
      setData(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };
  useEffect(() => {
    getAllPlan();
  }, []);

  const deletePlan = async (id) => {
    try {
      const result = await axios.delete(`${process.env.NEXT_PUBLIC_API_URL}/files/${id}`);
      console.log("🚀 ========= result:", result);
      const newResult = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/files`);
      console.log("🚀 ========= newResult:", newResult);
      setData(newResult?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };
  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f0f0f0] h-full min-h-screen flex-1">
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <button
              onClick={() => router.push("/curriculum/plan/create")}
              className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
            >
              Add plan
            </button>
          </div>
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <table className="w-full border-2 border-gray mx-auto bg-opacity-5">
              <thead className="bg-black bg-opacity-10">
                <tr>
                  <th className="w-1/4 border border-gray-300 p-2">
                    Plan name
                  </th>
                  <th className="border border-gray-300 flex justify-center gap-2 p-2">
                    Teacher
                  </th>
                  <th className="w-1/4 border border-gray-300 p-2">
                    Create at
                  </th>
                  <th className="w-1/4 border border-gray-300 p-2">Action</th>
                </tr>
              </thead>
              <tbody>
                {data?.map((item) => (
                  <tr key={item?._id}>
                    <td className="border border-gray-300">{item?.name}</td>
                    <td className="border border-gray-300">
                      {`${item?.user?.firstName ? item?.user?.firstName : ""} ${
                        item?.user?.lastName ? item?.user?.lastName : ""
                      }`}
                    </td>
                    <td className="border border-gray-300">
                      {dayjs(item?.createdAt).format("DD/MM/YYYY HH:mm:ss")}
                    </td>
                    <td className="flex gap-2 border border-gray-300 p-2">
                      <Button
                        variant="contained"
                        color="success"
                        onClick={() => deletePlan(item?._id)}
                      >
                        Delete
                      </Button>
                      <Link href={`${process.env.NEXT_PUBLIC_API_URL}/files/${item?._id}`}>
                        <Button variant="contained">Download</Button>
                      </Link>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </>
  );
}
