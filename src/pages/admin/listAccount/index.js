import React, { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Link from "next/link";
import { Button } from "@mui/material";
import Navbar from "../../../components/Navbar";
import withAuth from "@/middleware/middleware";
import decodeToken from "@/utils/auth";

function ListAccount() {
  const [data, setData] = useState();
  const router = useRouter();

  const [token, setToken] = useState();
  const getAccessToken = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    setToken(decode?.decoded?.data);
  };

  useEffect(() => {
    getAccessToken();
  }, []);

  const getAllUser = async () => {
    try {
      const result = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/accounts`
      );
      setData(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  useEffect(() => {
    getAllUser();
  }, []);

  return (
    <>
      <Navbar />
      {token?.user?.role?.roleName === "Admin" ? (
        <div className="flex w-full">
          <main className="p-2 bg-[#f0f0f0] h-full min-h-screen flex-1">
            <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
              <button
                onClick={() => router.push("/admin/addAccount")}
                className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
              >
                Add account
              </button>
            </div>
            <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
              <table className="w-full border-2 border-gray mx-auto bg-opacity-5">
                <thead className="bg-black bg-opacity-10">
                  <tr>
                    <th className="w-1/4 border border-gray-300 p-2">
                      Username
                    </th>
                    <th className="w-1/4 border border-gray-300 p-2">
                      Full name
                    </th>
                    <th className="w-1/4 border border-gray-300 p-2">Role</th>
                    <th className="w-1/4 border border-gray-300 p-2">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {data?.map((item) => (
                    <tr key={item?._id}>
                      <td className="border border-gray-300">
                        {item?.username}
                      </td>
                      <td className="border border-gray-300">
                        {item?.user?.firstName} {item?.user?.lastName}
                      </td>
                      <td className="border border-gray-300">
                        {item?.user?.role?.roleName}
                      </td>
                      <td className="flex gap-2 border border-gray-300 p-2">
                        <Link href={`changePassword/${item?._id}`}>
                          <Button variant="contained" color="success">
                            Change Password
                          </Button>
                        </Link>
                        <Link href={`changeProfile/${item?.user?._id}`}>
                          <Button variant="contained">Change profile</Button>
                        </Link>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </main>
        </div>
      ) : (
        <div className="text-center">
          <h1 className="text-5xl">Khong duoc phep truy cap</h1>
        </div>
      )}
    </>
  );
}
export default withAuth(ListAccount);
