import Navbar from "@/components/Navbar";
import withAuth from "@/middleware/middleware";
import axios from "axios";
import { useRouter } from "next/router";
import React from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

function ChangePassword() {
  const router = useRouter();
  const { id } = router.query;
  console.log("🚀 ========= router.query:", router.query);
  const { register, handleSubmit, setValue, control } = useForm();
  const changePassword = async (data) => {
    console.log("a", typeof id);
    try {
      const result = await axios.post(
        `${process.env.NEXT_PUBLIC_API_URL}/accounts/changePassword`,
        {
          id: id,
          password: data.password,
        }
      );
      console.log("🚀 ========= result:", result);
      toast.success(result?.data?.message, {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      router.push("/admin/listAccount");
      console.log("🚀 ========= result:", result?.data?.message);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };
  const onSubmit = (data) => {
    console.log("🚀 ========= data:", data.password);
    changePassword(data);
  };
  return (
    <div className="flex w-full">
      <main className="bg-[#f0f0f0] h-full min-h-screen flex-1">
        <Navbar />
        <div className="break-words w-11/12 m-auto mt-5 bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="w-full mx-auto mt-2 grid grid-cols-2 gap-4"
          >
            <div>
              <label className="block text-gray-700 text-sm font-bold mb-2">
                Password
              </label>
              <input
                type="password"
                {...register("password")}
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              />
            </div>
            <div colSpan={2} className="col-span-2">
              <button
                type="submit"
                className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              >
                Change Password
              </button>
            </div>
          </form>
        </div>
      </main>
    </div>
  );
}

export default withAuth(ChangePassword);
