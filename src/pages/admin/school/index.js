import Navbar from "@/components/Navbar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPenToSquare, faSort } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import axios from "axios";
import { useState, useEffect } from "react";

const Page = () => {
    const [listSchool, setListSchool] = useState([]);
    useEffect(() => {
        const getAllSchool = async () => {
            try {
                const result = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/schools`);
                setListSchool(result?.data?.data);
                console.log(listSchool);
            } catch (error) {
                console.log("error call school list api:", error);
            }
        };
        getAllSchool();
    }, []);

    return (
        <>
            <Navbar />
            <div className="flex w-full">
                <main className="p-2 bg-[#f4f6f9] h-full flex-1">
                    <div
                        className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex justify-end`}
                    >
                        <Link href="/admin/school/addSchool">
                            <button className="text-sm px-2 py-3 font-medium text-white bg-[#2BB56C] border border-[#2BB56C] hover:bg-[#00B153] rounded uppercase">
                                Add School
                            </button>
                        </Link>
                    </div>

                    <div
                        className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2`}
                    >
                        <div className="relative overflow-x-auto">
                            <table className="w-full text-md text-left rtl:text-right font-thin border">
                                <thead className="bg-[#f5f6fa] border-b">
                                    <tr>
                                        <th scope="col" className="px-6 py-3 font-thin border-r">
                                            No.
                                        </th>
                                        <th scope="col" className="px-6 py-3 font-thin border-r">
                                            School Name
                                        </th>
                                        <th scope="col" className="px-6 py-3 font-thin border-r">
                                            Address
                                        </th>
                                        <th scope="col" className="px-6 py-3 font-thin border-r">
                                            Phone Number
                                        </th>
                                        <th scope="col" className="px-6 py-3 font-thin text-center">
                                            Edit
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {listSchool?.map((listSchool, index) => (
                                        <tr key={listSchool?._id} className="bg-white border-b">
                                            <td className="px-6 py-4 whitespace-nowrap border-r">
                                                {index + 1}
                                            </td>
                                            <td
                                                scope="row"
                                                className="px-6 py-4 whitespace-nowrap border-r"
                                            >
                                                {listSchool?.schoolName}
                                            </td>
                                            <td className="px-6 py-4 border-r">
                                                {listSchool?.address}
                                            </td>
                                            <td className="px-6 py-4 border-r">
                                                {listSchool?.phone}
                                            </td>

                                            <td className="text-center">
                                                <Link href={`/admin/school/updateSchool/${listSchool?._id}`}>
                                                    <FontAwesomeIcon
                                                        icon={faPenToSquare}
                                                        className="text-sky-600 cursor-pointer hover:text-sky-800"
                                                    />
                                                </Link>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>

                    </div>
                </main>
            </div>
        </>
    );
}

export default Page
