import Navbar from "@/components/Navbar";
import Sidebar from "@/components/Sidebar";
import { EMAIL_REGEX, PHONE_NUMBER_REGEX } from "@/constants/regex";
import { MenuItem, Select } from "@mui/material";
import { useRouter } from "next/router";
import axios from "axios";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { useState, useEffect } from "react";
export default function Page() {
  const { register, handleSubmit, setValue, formState: { errors } } = useForm();
  const [school, setSchool] = useState({})
  const router = useRouter();
  const { id } = router.query;
  console.log("id", id);
  useEffect(() => {
    const getSchoolDetail = async () => {
      if (!router.isReady) return;
      try {
        const result = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/schools/${id}`);
        const schoolData = result?.data?.data;
        setSchool(schoolData)
        setValue("schoolName", schoolData?.schoolName);
        setValue("address", schoolData?.address);
        setValue("phoneNumber", schoolData?.phone);
        // console.log("school", school);
      } catch (error) {
        console.log("error call school detail api:", error);
      }
    }
    getSchoolDetail();
  }, [id, router.isReady]);
  const onSubmit = (data) => {
    updateSchool(data);
  }

  const updateSchool = async (data) => {
    console.log("data school:", data);
    try {
      const result = await axios.patch(`${process.env.NEXT_PUBLIC_API_URL}/schools/${id}`,
        data);
      console.log("🚀 ========= result:", result);
      if (result) {
        toast.success("Update Successfully");
      }
    } catch (error) {
      console.log("🚀 ========= error:", error);
      toast.error("Update Error");
    }
  }

  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f4f6f9] h-full font-thin flex-1">
          <div
            className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2 `}
          >
            <p className="font-thin text-xl mb-3">Update school information</p>
            <hr className="mb-4" />
            <form
              onSubmit={handleSubmit(onSubmit)}
              className="flex flex-col gap-4"
            >
              <p className="text-[#0497D8] uppercase">1. School information</p>
              <div className="flex gap-4">
                <div className="flex flex-col gap-2 flex-1">
                  <label htmlFor="schoolName">
                    School Name <span className="text-rose-500">(*)</span>
                  </label>
                  <input
                    {...register("schoolName", { required: true })}
                    className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  />
                </div>
                <div className="flex flex-col gap-2 flex-1">
                  <label htmlFor="lastName">
                    Address <span className="text-rose-500">(*)</span>
                  </label>
                  <input
                    {...register("address", { required: true })}
                    className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  />
                </div>
              </div>
              <div className="flex gap-4">
                <div className="flex flex-col gap-2 flex-1">
                  <label>Phone Number<span className="text-rose-500">(*)</span></label>
                  <input
                    {...register("phoneNumber", {
                      required: true, pattern: {
                        value: PHONE_NUMBER_REGEX,
                        message: "The phone number format is invalid",
                      },
                    })}
                    className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                  />
                  {errors.phoneNumber && (
                    <span className="text-rose-500 text-sm">
                      {errors.phoneNumber.message}
                    </span>
                  )}
                </div>
              </div>
              <div className="flex justify-end">
                <button
                  type="submit"
                  className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
                >
                  Save
                </button>
              </div>
            </form>
          </div>
        </main>
      </div>
    </>
  );
}


