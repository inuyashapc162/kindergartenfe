import Navbar from "@/components/Navbar";
import axios from "axios";
import { useRouter } from "next/router";
import React from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

const CreateAccount = () => {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    try {
      const result = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/accounts`, data);
      toast.success("Create account successfully!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      router.push("/admin/listAccount");
      console.log("🚀 ========= result:", result);
    } catch (error) {
      toast.error("Account exist!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      console.log("🚀 ========= error:", error);
    }
  };

  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f0f0f0] h-full min-h-screen flex-1">
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <form
              onSubmit={handleSubmit(onSubmit)}
              className="w-full mx-auto my-4 grid grid-cols-2 gap-4"
            >
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Username
                </label>
                <input
                  type="text"
                  {...register("username", { required: true })}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
                {errors.username && (
                  <span className="text-red-600">This field is required</span>
                )}
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Password
                </label>
                <input
                  type="text"
                  {...register("password", { required: true })}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
                {errors.password && (
                  <span className="text-red-600">This field is required</span>
                )}
              </div>
              <div colSpan={2} className="col-span-2">
                <button
                  type="submit"
                  className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
                >
                  Create account
                </button>
              </div>
            </form>
          </div>
        </main>
      </div>
    </>
  );
};

export default CreateAccount;
