import Navbar from "@/components/Navbar";
import decodeToken from "@/utils/auth";
import axios from "axios";
import Image from "next/image";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-toastify";

const ChangeProfileForm = () => {
  const router = useRouter();
  const { id } = router.query;

  const [data, setData] = useState();
  const [roles, setRoles] = useState();
  const [schools, setSchools] = useState();
  const [classes, setClasses] = useState();
  const [token, setToken] = useState();
  const [selectedSchool, setSelectedSchool] = useState(null);
  const [selectedClass, setSelectedClass] = useState(null);
  const [schoolData, setSchoolData] = useState([]);
  console.log("🚀 ========= schoolData:", schoolData);
  const [classData, setClassData] = useState([]);

  const { register, handleSubmit, setValue, control } = useForm();
  const getAccessToken = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    setToken(decode?.decoded?.data);
  };

  useEffect(() => {
    getAccessToken();
  }, []);
  const onSubmit = (data1) => {
    changeProfile(data1);
  };

  const getProfileDetail = async () => {
    try {
      const result = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/users/${id}`
      );
      console.log("🚀 ========= result:", result);
      setData(result?.data?.data);
      setValue("firstName", result?.data?.data?.firstName);
      setValue("lastName", result?.data?.data?.lastName);
      setValue("address", result?.data?.data?.address);
      setValue("phoneNumber", result?.data?.data?.phoneNumber);
      setValue("email", result?.data?.data?.email);
      setValue("role", result?.data?.data?.role);
      setValue("schoolID", result?.data?.data?.schoolID);
      setValue("classID", result?.data?.data?.classID);

      return result;
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  const changeProfile = async (data) => {
    console.log("🚀 ========= data:", data);
    const {
      firstName,
      lastName,
      address,
      role,
      phoneNumber,
      email,
      schoolID,
      classID,
    } = data;
    try {
      if (data.avatar) {
        uploadImage(data.avatar[0]);
      }
      const result = await axios.patch(
        `${process.env.NEXT_PUBLIC_API_URL}/users/${id}`,
        {
          firstName,
          lastName,
          address,
          role,
          phoneNumber,
          email,
          schoolID,
          classID,
        }
      );
      toast.success("Change profile success", {
        position: "top-right",
        autoClose: 3000, // Đóng sau 3 giây
      });
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  const uploadImage = async (image) => {
    const formData = new FormData();
    formData.append("avatar", image);
    try {
      const result = await axios.post(
        `${process.env.NEXT_PUBLIC_API_URL}/users/${id}`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      // router.push("/admin/listAccount");
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  const getRole = async () => {
    try {
      const result = await axios.get(
        process.env.NEXT_PUBLIC_API_URL + "/roles"
      );
      setRoles(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };
  const getSchool = async () => {
    try {
      const result = await axios.get(
        process.env.NEXT_PUBLIC_API_URL + "/schools",
        {
          headers: {
            token: `${JSON.parse(localStorage.getItem("token"))?.token} `,
          },
        }
      );
      setSchools(result?.data?.data);
      setSchoolData(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  const getClass = async (schoolId) => {
    try {
      const result = await axios.get(
        process.env.NEXT_PUBLIC_API_URL + "/classes/school",
        {
          headers: {
            token: `${JSON.parse(localStorage.getItem("token"))?.token} `,
          },
        }
      );
      setClasses(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };

  useEffect(() => {
    getProfileDetail();
    getRole();
    getSchool();
    // getClass();
  }, [id]);

  const handleChangeSchool = (e) => {
    console.log("🚀 ========= e:", e.target.value);
    setSelectedSchool(e.target.value);
    setValue("schoolID", e.target.value);
  };

  // Hàm gọi API để lấy danh sách lớp học dựa trên trường học đã chọn từ đầu
  const getClassBySchool = async () => {
    try {
      const result = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/classes/school/${selectedSchool}`,
        {
          headers: {
            token: `${JSON.parse(localStorage.getItem("token"))?.token} `,
          },
        }
      );
      setClassData(result?.data?.data);
    } catch (error) {
      console.error("Error fetching class data:", error);
    }
  };
  useEffect(() => {
    // Gọi hàm để lấy danh sách lớp học khi trang được tải lần đầu tiên
    if (selectedSchool) {
      getClassBySchool();
    }
  }, [selectedSchool]); // Kích hoạt lại khi trường học được chọn thay đổi

  useEffect(() => {
    // Gọi hàm để lấy danh sách lớp học khi trang được tải lần đầu tiên
    if (schoolData.length > 0) {
      setSelectedSchool(schoolData[0]._id); // Chọn trường học đầu tiên để hiển thị danh sách lớp học
    }
  }, [schoolData]); // Kích hoạt lại khi danh sách trường học được cập nhật

  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f0f0f0] h-full min-h-screen flex-1">
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <form
              onSubmit={handleSubmit(onSubmit)}
              className="w-full mx-auto mt-8 grid grid-cols-2 gap-4"
            >
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Preview
                </label>
                <Image
                  src={data?.avatar?.url}
                  width={200}
                  height={200}
                  alt="anh"
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Avatar
                </label>
                <input
                  type="file"
                  {...register("avatar")}
                  accept="image/*"
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Firstname
                </label>
                <input
                  type="text"
                  {...register("firstName")}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Lastname
                </label>
                <input
                  type="text"
                  {...register("lastName")}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Address
                </label>
                <input
                  type="text"
                  {...register("address")}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                  // setValue=('address', data?.address)
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Role
                </label>
                <Controller
                  name="role"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <select
                      {...field}
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    >
                      <option value="" disabled>
                        Select a role
                      </option>
                      {roles?.map((item) => (
                        <option key={item._id} value={item._id}>
                          {item.roleName}
                        </option>
                      ))}
                    </select>
                  )}
                  disabled={token?.user?.role?.roleName != "Admin"}
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Phone number
                </label>
                <input
                  type="number"
                  {...register("phoneNumber")}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Email
                </label>
                <input
                  type="email"
                  {...register("email")}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  School
                </label>
                <Controller
                  name="schoolID"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <select
                      {...field}
                      onChange={handleChangeSchool}
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    >
                      <option value="" disabled>
                        Select a role
                      </option>
                      {schools?.map((item) => (
                        <option key={item._id} value={item._id}>
                          {item.schoolName}
                        </option>
                      ))}
                    </select>
                  )}
                  disabled={token?.user?.role?.roleName != "Admin"}
                />
              </div>
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Class
                </label>
                <Controller
                  name="classID"
                  control={control}
                  defaultValue=""
                  render={({ field }) => (
                    <select
                      {...field}
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                    >
                      <option value="" disabled>
                        Select a class
                      </option>
                      {classData?.map((item) => (
                        <option key={item._id} value={item._id}>
                          {item.className}
                        </option>
                      ))}
                    </select>
                  )}
                  disabled={token?.user?.role?.roleName != "Admin"}
                />
              </div>

              <div colSpan={2} className="col-span-2">
                <button
                  type="submit"
                  className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                >
                  Change Profile
                </button>
              </div>
            </form>
          </div>
        </main>
      </div>
    </>
  );
};

export default ChangeProfileForm;
