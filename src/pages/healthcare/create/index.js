import Navbar from "@/components/Navbar";
import decodeToken from "@/utils/auth";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";

export default function CreateHealthCare() {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [token, setToken] = useState();
  const getAccessToken = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    setToken(decode?.decoded?.data);
  };

  useEffect(() => {
    getAccessToken();
  }, []);
  const onSubmit = async (data) => {
    console.log("🚀 ========= data:", data.HealthReportName);
    try {
      const result = await axios.put(
        `${process.env.NEXT_PUBLIC_API_URL}/healthcare/initializeHealthReportDetail`,
        {
          classID: token?.user?.classID,
          HealthReportName: data?.HealthReportName,
        }
      );
      toast.success("Create account successfully!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      router.push("/healthcare");
      console.log("🚀 ========= result:", result);
    } catch (error) {
      toast.error("Account exist!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      console.log("🚀 ========= error:", error);
    }
  };
  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f0f0f0] h-full min-h-screen flex-1">
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <form
              onSubmit={handleSubmit(onSubmit)}
              className="w-full mx-auto my-4 grid grid-cols-2 gap-4"
            >
              <div>
                <label className="block text-gray-700 text-sm font-bold mb-2">
                  Health report name
                </label>
                <input
                  type="text"
                  {...register("HealthReportName", { required: true })}
                  className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                />
                {errors.HealthReportName && (
                  <span className="text-red-600">This field is required</span>
                )}
              </div>
              <div colSpan={2} className="col-span-2">
                <button
                  type="submit"
                  className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
                >
                  Create report
                </button>
              </div>
            </form>
          </div>
        </main>
      </div>
    </>
  );
}
