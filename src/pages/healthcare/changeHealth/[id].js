import { ExportToExcel } from "@/components/Common/ExportToExcel";
import Navbar from "@/components/Navbar";
import decodeToken from "@/utils/auth";
import axios from "axios";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";

export default function ChangeHealth() {
  const router = useRouter();
  const { id } = router.query;
  const [healthCare, setHealthCare] = useState();
  const [data, setData] = useState([]);
  const [healthDetailData, setHealthDetailData] = useState([]);
  const [selectedStatus, setSelectedStatus] = useState({});
  const [healthData, setHealthData] = useState([]);
  const [healthDataSelected, setHealthDataSelected] = useState([]);
  const [healthCareData, setHealthCareData] = useState();
  const [token, setToken] = useState();
  console.log("🚀 ========= token1234:", token);
  const getAccessToken = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    console.log("🚀 ========= decode:", decode);
    setToken(decode?.decoded?.data);
  };
  console.log("🚀 ========= healthDataSelected:", healthDataSelected);
  console.log("🚀 ========= healthData:", healthData);
  const fileName = "HealthCare";
  const getData = async () => {
    try {
      const result = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/healthcare/${id}`
      );
      console.log("🚀 ========= HealthCare:", result?.data?.data);
      setData(result?.data?.data?.HealthReportDetail);
      setHealthCare(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };
  useEffect(() => {
    getAccessToken();
  }, []);
  useEffect(() => {
    getData();
  }, [id]);

  useEffect(() => {
    // Tạo mảng các đối tượng mới chứa id, height và weight từ mảng dữ liệu
    const newHealthData = data?.map((item) => ({
      id: item._id,
      height: item.height,
      weight: item.weight,
    }));
    console.log("🚀 ========= newHealthData:", data);

    setHealthData(newHealthData);
  }, [data]);

  // Xử lý sự kiện thay đổi giá trị của trường height
  const handleHeightChange = (index, value) => {
    const newHealthData = [...healthData];
    newHealthData[index].height = value;
    setHealthData(newHealthData);
  };

  // Xử lý sự kiện thay đổi giá trị của trường weight
  const handleWeightChange = (index, value) => {
    const newHealthData = [...healthData];
    newHealthData[index].weight = value;
    setHealthData(newHealthData);
  };

  const changeHealthData = async (e) => {
    e.preventDefault();

    try {
      const requests = healthData.map((data) => {
        return axios.put(
          `${process.env.NEXT_PUBLIC_API_URL}/healthcare/changeHealthReportDetail`,
          {
            id: data.id,
            weight: data.weight,
            height: data.height,
          }
        );
      });

      await Promise.all(requests);
      toast.success("Change health saved successfully", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      console.log("Change health saved successfully.");
    } catch (error) {
      console.error("Error saving attendance:", error);
    }
  };

  useEffect(() => {
    const customHeadings = data?.map((item, index) => ({
      Id: index + 1,
      FullName: item?.Student?.fullName,
      Gender: item?.Student?.gender ? "Nam" : "Nữ",
      DateOfBirth: dayjs(item?.Student?.dateOfBirth).format("DD/MM/YYYY"),
      Height: item?.height,
      Weight: item?.weight,
    }));
    setHealthDataSelected(customHeadings);
  }, [data]);
  return (
    <div className="bg-[#f0f0f0] h-full min-h-screen flex-1">
      <Navbar />
      <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-4 shadow-md mt-4 flex justify-between items-center">
        <div>
          <h1 className="font-bold text-2xl">{healthCare?.HealthReportName}</h1>
          <h1 className="font-normal text-xl">
            Ngày cân đo: {dayjs(healthCare?.createdAt).format("DD/MM/YYYY")}
          </h1>
          <h1 className="font-normal text-xl">
            Phạm vi : {healthCare?.Class?.className}
          </h1>
        </div>
        <ExportToExcel
          apiData={healthDataSelected}
          fileName={fileName}
          className={
            "bg-green-500 p-2 rounded-xl flex gap-2 items-center h-12 justify-center"
          }
        />
      </div>
      <form
        className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 shadow-md"
        onSubmit={changeHealthData}
      >
        <table className="w-full border-2 border-gray mx-auto bg-opacity-5">
          <thead className="bg-black bg-opacity-10">
            <tr>
              <th className="w-1/12 border border-gray-300 p-2">STT</th>
              <th className="w-1/4 border border-gray-300 p-2">Ho va ten</th>
              <th className="w-1/12 border border-gray-300 p-2">Gioi tinh</th>
              <th className="w-1/6 border border-gray-300 p-2">Nam sinh</th>
              <th className="w-1/8 border border-gray-300 p-2">Chieu cao</th>
              <th className="w-1/12 border border-gray-300 p-2">Tinh trang</th>
              <th className="w-1/8 border border-gray-300 p-2">Can nang</th>
              <th className="w-1/12 border border-gray-300 p-2">Tinh trang</th>
            </tr>
          </thead>
          <tbody>
            {data?.map((item, index) => (
              <tr key={item?._id}>
                <td className="border border-gray-300">{index + 1}</td>
                <td className="border border-gray-300">
                  {item?.Student?.fullName}
                </td>
                <td className="border border-gray-300">
                  {item?.Student?.gender ? "Nam" : "Nu"}
                </td>
                <td className="border border-gray-300">
                  {dayjs(item?.Student?.dateOfBirth).format("DD/MM/YYYY")}
                </td>
                <td className="border border-gray-300 text-[#0497D8] flex">
                  <input
                    type="number"
                    defaultValue={item?.height}
                    onChange={(e) => handleHeightChange(index, e.target.value)}
                  />
                  cm
                </td>
                <td className="border border-gray-300">Binh thuong</td>
                <td className="border border-gray-300 text-[#0497D8] flex">
                  <input
                    type="number"
                    defaultValue={item.weight}
                    onChange={(e) => handleWeightChange(index, e.target.value)}
                  />
                  kg
                </td>
                <td className="border border-gray-300">Binh thuong</td>
              </tr>
            ))}
          </tbody>
        </table>
        {token?.user?.role?.roleName !== "Parent" && (
          <div className="flex justify-end mt-4">
            <button
              type="submit"
              className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
            >
              Change heath data
            </button>
          </div>
        )}
      </form>
    </div>
  );
}
