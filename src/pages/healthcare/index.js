import React, { useEffect, useState } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import Link from "next/link";
import { Button } from "@mui/material";
import withAuth from "@/middleware/middleware";
import Navbar from "@/components/Navbar";
import dayjs from "dayjs";
import { toast } from "react-toastify";
import decodeToken from "@/utils/auth";
function ListAccount() {
  const [data, setData] = useState();
  const [deteleData, setDeleteData] = useState();
  const [token, setToken] = useState();
  const router = useRouter();
  console.log("🚀 ========= token1234:", token);
  const getAccessToken = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    setToken(decode?.decoded?.data);
  };
  useEffect(() => {
    getAccessToken();
  }, []);
  const getAllUser = async () => {
    try {
      const result = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/healthcare`
      );
      console.log("🚀 ========= result:", result?.data?.data);
      setData(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };
  useEffect(() => {
    getAllUser();
  }, [deteleData]);

  const handleDeleteReport = async (id) => {
    try {
      const result = await axios.delete(
        `${process.env.NEXT_PUBLIC_API_URL}/healthcare/${id}`
      );
      console.log("🚀 ========= result:", result);
      toast.success(result?.data?.message, {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
      setDeleteData(result?.data?.data);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };
  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f0f0f0] h-full min-h-screen flex-1">
          {token?.user?.role?.roleName !== 'Parent' &&
            <div
              className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md"
            >
              <button
                onClick={() => router.push("/healthcare/create")}
                className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"

              >
                Add health report
              </button>
            </div>}

          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <table className="w-full border-2 border-gray mx-auto bg-opacity-5">
              <thead className="bg-black bg-opacity-10">
                <tr>
                  <th className="w-1/12 border border-gray-300 p-2">No</th>
                  <th className="w-1/4 border border-gray-300 p-2">
                    Health report name
                  </th>
                  <th className="w-1/4 border border-gray-300 p-2">Class</th>
                  <th className="w-1/4 border border-gray-300 p-2">
                    Created at
                  </th>
                  <th className="w-1/6 border border-gray-300 p-2">Status</th>
                  <th className="w-1/6 border border-gray-300 p-2">Action</th>
                </tr>
              </thead>
              <tbody>
                {data?.map((item, index) => (
                  <tr key={item?._id}>
                    <td className="border border-gray-300">{index + 1}</td>
                    <td className="border border-gray-300">
                      {item?.HealthReportName}
                    </td>
                    <td className="border border-gray-300">
                      {item?.Class?.className}
                    </td>
                    <td className="border border-gray-300">
                      {dayjs(item?.createdAt).format("DD/MM/YYYY HH:mm:ss")}
                    </td>
                    <td className="border border-gray-300">
                      {item?.isStatus ? (
                        <p className="text-green-400 font-bold">Hoat dong</p>
                      ) : (
                        <p className="text-red-400 font-bold">Chua hoat dong</p>
                      )}
                    </td>
                    {token?.user?.role?.roleName !== "Parent" ? (
                      <td className="flex gap-2 border border-gray-300 p-2">
                        <Link href={`/healthcare/changeHealth/${item?._id}`}>
                          <Button variant="contained" color="success">
                            Edit
                          </Button>
                        </Link>
                        <Button
                          variant="contained"
                          onClick={() => handleDeleteReport(item?._id)}
                        >
                          Delete
                        </Button>
                      </td>
                    ) : (
                      <td className="flex gap-2 border border-gray-300 p-2">
                        <Link href={`/healthcare/changeHealth/${item?._id}`}>
                          <Button variant="contained" color="success">
                            View
                          </Button>
                        </Link>
                      </td>
                    )}
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </main>
      </div>
    </>
  );
}
export default withAuth(ListAccount);
