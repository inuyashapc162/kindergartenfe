import axios from "axios";
import React from "react";
import { useForm } from "react-hook-form";

export default function TestUpload() {
  const { register, handleSubmit, setValue, control } = useForm();
  const uploadImage = async (image) => {
    console.log("🚀 ========= image:", image);
    const formData = new FormData();
    formData.append("avatar", image);
    try {
      const result = await axios.post(
        `${process.env.NEXT_PUBLIC_API_URL}/images`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      console.log("🚀 ========= result:", result);
    } catch (error) {
      console.log("🚀 ========= error:", error);
    }
  };
  const onSubmit = (data) => {
    // console.log("Submitted data1234:", data?.firstName);
    // changeProfile(data1);
    uploadImage(data.avatar[0]);
    console.log("Submitted data:", data.avatar[0]);
  };
  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="max-w-2xl mx-auto mt-8 grid grid-cols-2 gap-4"
    >
      <div>
        <label className="block text-gray-700 text-sm font-bold mb-2">
          Avatar
        </label>
        <input
          type="file"
          {...register("avatar")}
          accept="image/*"
          className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        />
      </div>
      <div colSpan={2} className="col-span-2">
        <button
          type="submit"
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        >
          Change Profile
        </button>
      </div>
    </form>
  );
}
