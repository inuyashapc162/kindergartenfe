import Navbar from "@/components/Navbar";
import axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import edit from "../../../public/img/icon-edit.png";
import view from "../../../public/img/icon-view.png";
import Image from "next/image";
import decodeToken from "@/utils/auth";
import ethnicGroup from "@/constants/ethnicgroup";
import status from "@/constants/status";

const Student = () => {
  const router = useRouter();
  const [listStudent, setListStudent] = useState([]);
  const [listClass, setListClass] = useState();
  const [selectClass, setSelectClass] = useState();
  const [user, setUser] = useState([]);
  const getDate = (dateOfBirth) => {
    if (!dateOfBirth) return "";
    const options = { year: "numeric", month: "numeric", day: "numeric" };
    return new Date(dateOfBirth).toLocaleDateString("vi-VI", options);
  };


  useEffect(() => {
    const fetchData = async () => {
      const token = localStorage.getItem("token");
      if (token) {
        try {
          const decode = await decodeToken(JSON.parse(token)?.token);
          console.log("decode", decode);
          setUser(decode?.decoded?.data?.user);
        } catch (error) {
          console.error("Error decoding token:", error);
        }
      }
    };
    fetchData();
  }, []);

  // const getAllClass = async () => {
  //   const token = localStorage.getItem("token");
  //   const decode = await decodeToken(JSON.parse(token)?.token);
  //   const schoolID = (decode?.decoded?.data?.user?.schoolID);
  //   try {
  //     const result = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/classes/school/${schoolID}`,
  //       {
  //         headers: {
  //           "token": JSON.parse(token).token,
  //         },
  //       }
  //     );

  //     setListClass(result?.data?.data);
  //   } catch (error) {
  //     console.log("🚀 ========= error:", error);
  //   }
  // };

  // useEffect(() => {
  //   getAllClass();
  // }, []);

  const getAllStudent = async () => {
    console.log("classid", user?.classID);
    try {
      const result = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/students`,
        {
          params: {
            classID: user?.classID
          }
        });
      console.log("listStudent", result);
      setListStudent(result?.data?.data?.result);
      setListClass(result?.data?.data?.classData[0]);
    } catch (error) {
      console.log("error:", error);
    }
  };

  useEffect(() => {
    getAllStudent();
  }, [selectClass, user]);

  const getEthnicGroupValue = (key) => {
    const mapping = ethnicGroup.find(item => item.id == key);
    return mapping ? mapping.value : '';
  };

  const getStatus = (key) => {
    const mapping = status.find(item => item.id == key);
    return mapping ? mapping.value : '';
  };

  const handleChangeClass = (e) => {
    console.log("classchange", e.target.value);
    setSelectClass(e.target.value)
  }
  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f0f0f0] h-full min-h-screen flex-1">
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">

            <div className="flex flex-row gap-2 items-center">
              <label className="font-bold">
                CLASS : <span className="text-blue-500 uppercase">{listClass?.className}</span>
              </label>
              {/* {user?.classID === undefined && (
                <select className="p-2 border rounded-md" onChange={handleChangeClass}>
                  {listClass?.map(lc => (
                    <options key={lc._id} value={lc._id}>{lc.className}</options>
                  ))}
                </select>
              )}
              {user?.classID !== undefined && (
                <select className="p-2 border rounded-md">
                  {listClass?.filter(lc => lc._id === user.classID).map(lc => (
                    <option key={lc._id} value={lc._id}>{lc.className}</option>
                  ))}
                </select>
              )} */}
            </div>
            {
              user?.role?.roleName !== "Parent" && (
                <button
                  onClick={() => router.push("/student/create")}
                  className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
                >
                  Add Student
                </button>
              )
            }


          </div>
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <table className="w-full border-2 border-gray mx-auto bg-opacity-5">
              <thead className="bg-black bg-opacity-10">
                <tr>
                  <th className="border border-gray-300 p-2">No.</th>
                  <th className="border border-gray-300 p-2">Full Name</th>
                  <th className="border border-gray-300 p-2">Date Of Birth</th>
                  <th className="border border-gray-300 p-2">Gender</th>
                  <th className="border border-gray-300 p-2">Ethnic</th>
                  <th className="border border-gray-300 p-2">Status</th>
                  <th className="border border-gray-300 p-2">Action</th>
                </tr>
              </thead>
              <tbody>
                {listStudent?.map((item, index) => (
                  <tr key={item?._id}>
                    <td className="border border-gray-300 text-center">
                      {index + 1}
                    </td>
                    <td className="border border-gray-300 text-center">
                      {item?.fullName}
                    </td>
                    <td className="border border-gray-300 text-center">
                      {getDate(item?.dateOfBirth)}
                    </td>
                    <td className="border border-gray-300 text-center">
                      {item?.gender ? "Male" : "Female"}
                    </td>
                    <td className="border border-gray-300 text-center">
                      {getEthnicGroupValue(item?.ethnicGroups)}
                    </td>
                    <td className="border border-gray-300 text-center">
                      {getStatus(item?.status)}
                    </td>
                    <td className="border border-gray-300 text-center">
                      <div className="flex items-center justify-center p-1">
                        <Link href={`/student/update/${item?._id}`}>
                          <Image width={18} height={18} src={view} alt='View Image' />
                        </Link>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </main >

      </div >
    </>
  );
};

export default Student;
