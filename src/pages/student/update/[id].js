import Navbar from "@/components/Navbar";
import React, { useState, useEffect } from "react";
import axios from "axios";
import { useRouter } from "next/router";
import { toast } from "react-toastify";
import { Controller, useForm } from "react-hook-form";
import { DAYCARE_CENTER, KINDERGARTEN } from '../../../constants/grade.js';
import { FEMALE, MALE } from '../../../constants/gender.js';
import { Tab } from '@headlessui/react';
import dayjs from 'dayjs';
import ethnicGroup from '@/constants/ethnicgroup.js';
import nationality from '@/constants/nationality.js';
import status from '@/constants/status.js';
import religion from '@/constants/religion.js';
import disabilities from '@/constants/disable.js';
import decodeToken from "@/utils/auth.js";

const CreateStudent = () => {
  const [studentDetail, setStudentDetail] = useState();
  const [user, setUser] = useState();
  const router = useRouter();
  const { id } = router.query;
  console.log("id", id);
  const { register, handleSubmit, setValue, control } = useForm();
  const [province, setProvince] = useState();
  const [district, setDistrict] = useState();
  const [ward, setWard] = useState();
  const [province2, setProvince2] = useState();
  const [district2, setDistrict2] = useState();
  const [ward2, setWard2] = useState();
  const [listProvince, setListProvince] = useState();
  const [listDistrict, setListDistrict] = useState();
  const [listWard, setListWard] = useState();
  const [listDistrict2, setListDistrict2] = useState();
  const [listWard2, setListWard2] = useState();
  const currentDate = new Date().toISOString().split("T")[0];
  useEffect(() => {
    const fetchData = async () => {
      const token = localStorage.getItem("token");
      if (token) {
        try {
          const decode = await decodeToken(JSON.parse(token)?.token);
          console.log("decode", decode);
          setUser(decode?.decoded?.data?.user);
        } catch (error) {
          console.error("Error decoding token:", error);
        }
      }
    };
    fetchData();
  }, []);

  const getStudentByID = async () => {
    if (id) {
      try {
        const result = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/students/${id}`);
        const student = result?.data?.data[0];
        console.log("test", student?.temporaryAddressID?.province);
        console.log("studentData", result);
        setStudentDetail(student);
        setValue("fullName", student?.fullName),
          setValue("gender", student?.gender),
          setValue("grade", student?.grade),
          setValue("status", student?.status),
          setValue("dateOfBirth", dayjs(student?.dateOfBirth).format('YYYY-MM-DD')),
          setValue("admissionDay", dayjs(student?.admissionDay).format('YYYY-MM-DD')),
          setValue("ethnicGroups", student?.ethnicGroups),
          setValue("nationality", student?.nationality),
          setValue("religion", student?.religion),
          setValue("identifier", student?.identifier),
          setValue("issueDate", dayjs(student?.issueDate).format('YYYY-MM-DD')),
          setValue("issuePlace", student?.issuePlace),
          setValue("province", student?.temporaryAddressID?.province),
          setProvince(student?.temporaryAddressID?.province)
        setValue("district", student?.temporaryAddressID?.district),
          setDistrict(student?.temporaryAddressID?.district)
        setValue("subDistrict", student?.temporaryAddressID?.subDistrict),
          setWard(student?.temporaryAddressID?.subDistrict)
        setValue("village", student?.temporaryAddressID?.village),
          setValue("nickName", student?.studentDetailID?.nickName),
          setValue("isDisabled", student?.studentDetailID?.isDisabled),
          setValue("ethnicMinorityFather", student?.studentDetailID?.ethnicMinorityFather),
          setValue("ethnicMinorityMother", student?.studentDetailID?.ethnicMinorityMother),
          setValue("isTwoSession", student?.studentDetailID?.isTwoSession),
          setValue("isBeneficiaryOfSocialWelfare", student?.studentDetailID?.isBeneficiaryOfSocialWelfare),
          setValue("isCanSwim", student?.studentDetailID?.isCanSwim),
          setValue("isDayBoarding", student?.studentDetailID?.isDayBoarding),
          // setValue("province2", student?.permanentAddressID?.province),
          // setValue("district2", student?.permanentAddressID?.district),
          // setValue("subDistrict2", student?.permanentAddressID?.subDistrict),
          setProvince2(student?.permanentAddressID?.province);
        setDistrict2(student?.permanentAddressID?.district)
        setWard2(student?.permanentAddressID?.subDistrict)
        setValue("placeOfBirth", student?.permanentAddressID?.placeOfBirth)

      } catch (error) {
        console.log("error:", error);
      }
    }

  };

  useEffect(() => {
    getStudentByID();
  }, [id]);

  useEffect(() => {
    //province
    const getProvince = async () => {
      try {
        const result = await axios.get(
          `https://api.npoint.io/ac646cb54b295b9555be`
        );
        setListProvince(result?.data);
        // console.log("province", result);
      } catch (error) {
        console.log("🚀 ========= error:", error);
      }
    };
    getProvince();
  }, []);

  useEffect(() => {
    //district
    const getDistrict = async (province) => {
      try {
        const result = await axios.get(
          `https://api.npoint.io/34608ea16bebc5cffd42`
        );
        const districtRaw = [].concat(...result?.data);
        setListDistrict(districtRaw.filter((a) => a.ProvinceId == province));
      } catch (error) {
        console.log("🚀 ========= error:", error);
      }
    };
    getDistrict(province);
  }, [province]);

  useEffect(() => {
    //district
    const getDistrict = async (province2) => {
      try {
        const result = await axios.get(
          `https://api.npoint.io/34608ea16bebc5cffd42`
        );
        const districtRaw = [].concat(...result?.data);
        setListDistrict2(districtRaw.filter((a) => a.ProvinceId == province2));
      } catch (error) {
        console.log("🚀 ========= error:", error);
      }
    };
    getDistrict(province2);
  }, [province2]);


  useEffect(() => {
    //ward
    const getWard = async (district) => {
      try {
        const result = await axios.get(
          `https://api.npoint.io/dd278dc276e65c68cdf5`
        );
        // console.log("ward", result);
        const wardRaw = [].concat(...result?.data);
        setListWard(wardRaw.filter((a) => a.DistrictId == district));
      } catch (error) {
        console.log("🚀 ========= error:", error);
      }
    };
    getWard(district);
  }, [district]);

  useEffect(() => {
    //ward
    const getWard = async (district2) => {
      try {
        const result = await axios.get(
          `https://api.npoint.io/dd278dc276e65c68cdf5`
        );
        // console.log("ward", result);
        const wardRaw = [].concat(...result?.data);
        setListWard2(wardRaw.filter((a) => a.DistrictId == district2));
      } catch (error) {
        console.log("🚀 ========= error:", error);
      }
    };
    getWard(district2);
  }, [district2]);

  const onSubmit = (data) => {
    console.log("data", data);
    // console.log("permanentAddressID", studentDetail);
    const newData = {
      studentID: studentDetail?._id,
      permanentAddressID: studentDetail?.permanentAddressID?._id,
      temporaryAddressID: studentDetail?.temporaryAddressID?._id,
      studentDetailID: studentDetail?.studentDetailID?._id,
      dataStudent: {
        fullName: data?.fullName,
        grade: data?.grade,
        classID: user?.classID,
        dateOfBirth: data.dateOfBirth,
        gender: data?.gender,
        status: data?.status,
        admissionDay: data?.admissionDay,
        ethnicGroups: data?.ethnicGroups,
        nationality: data?.nationality,
        religion: data?.religion,
        identifier: data?.identifier,
        issueDate: data?.issueDate,
        issuePlace: data?.issuePlace,
      },
      dataStudentDetail: {
        nickName: data?.nickName,
        isDisabled: data?.isDisabled,
        ethnicMinorityFather: data?.ethnicMinorityFather,
        ethnicMinorityMother: data?.ethnicMinorityMother,
        isTwoSession: data?.isTwoSession === "true" ? true : false,

        isBeneficiaryOfSocialWelfare:
          data?.isBeneficiaryOfSocialWelfare == "true" ? true : false,
        isCanSwim: data?.isCanSwim === "true" ? true : false,
        isDayBoarding: data?.isDayBoarding === "true" ? true : false,
      },
      dataTemAdd: {
        province: data?.province,
        subDistrict: data?.subDistrict,
        district: data?.district,
        village: data?.village,
      },
      dataPerAdd: {
        province: data?.province2,
        subDistrict: data?.subDistrict2,
        district: data?.district2,
        placeOfBirth: data?.placeOfBirth,
      },
    };
    updateStudent(newData);
  };

  const updateStudent = async (data) => {
    console.log("data school:", data);
    try {
      const result = await axios.patch(`${process.env.NEXT_PUBLIC_API_URL}/students`, data);
      console.log("🚀 ========= result:", result);
      if (result) {
        toast.success("Update Successfully");
      }
    } catch (error) {
      console.log("🚀 ========= error:", error);
      toast.error("Update Error");
    }
  };

  const handleProvinceChange = (event) => {
    setProvince(event.target.value);
  };

  const handleDistrictChange = (event) => {
    setDistrict(event.target.value);
  };
  const handleProvinceChange2 = (event) => {
    setProvince2(event.target.value);
  };

  const handleDistrictChange2 = (event) => {
    setDistrict2(event.target.value);
  };
  function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
  }
  return (
    <>
      <Navbar />
      <div className="flex w-full">
        {/* <Sidebar /> */}
        <main className="p-2 bg-[#f4f6f9] h-full font-thin flex-1">
          <div
            className={`break-words bg-[#fff] bg-clip-border border rounded p-4 mb-2 `}
          >
            <p className="font-thin text-xl mb-3">Update Student</p>
            <hr className="mb-4" />
            <form
              onSubmit={handleSubmit(onSubmit)}
              className="flex flex-col gap-4"
            >
              <Tab.Group>
                <Tab.List className="flex space-x-1 rounded-xl bg-blue-900/20 p-1">
                  <Tab
                    className={({ selected }) =>
                      classNames(
                        "w-full rounded-lg py-2.5 text-base font-bold leading-5",
                        "ring-white/60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2",
                        selected
                          ? "bg-blue-200 text-blue-700 shadow"
                          : "bg-white text-blue-700 hover:bg-blue-500 hover:text-white"
                      )
                    }
                  >
                    Main Information
                  </Tab>
                  <Tab
                    className={({ selected }) =>
                      classNames(
                        "w-full rounded-lg py-2.5 text-base font-bold leading-5",
                        "ring-white/60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2",
                        selected
                          ? "bg-blue-200 text-blue-700 shadow"
                          : "bg-white text-blue-700 hover:bg-blue-500 hover:text-white"
                      )
                    }
                  >
                    Addition Information
                  </Tab>
                </Tab.List>
                <Tab.Panels className="mt-2">
                  <Tab.Panel
                    className={classNames(
                      "rounded-xl bg-white p-3",
                      "ring-white/60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2"
                    )}
                  >
                    <p className="text-[#0497D8] uppercase">
                      1. General Information
                      <span className="text-rose-500">(*)</span>
                    </p>
                    <div className="flex flex-col gap-4">
                      <div className="grid grid-cols-4 gap-2 flex-1">
                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="firstName">
                              Full Name{" "}
                              <span className="text-rose-500">(*)</span>
                            </label>
                            <input readOnly={user?.role?.roleName === "Parent"}
                              {...register("fullName", { required: true })}
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                            />
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="lastName">
                              Gender <span className="text-rose-500">(*)</span>
                            </label>

                            <select
                              {...register("gender", { required: true })}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}
                            >
                              <option value="" disabled>
                                Select gender
                              </option>
                              <option value={MALE}>Male</option>
                              <option value={FEMALE}>Female</option>
                            </select>
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="lastName">
                              Grade <span className="text-rose-500">(*)</span>
                            </label>
                            <select
                              {...register("grade", { required: true })}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}
                            >
                              <option value="" disabled>
                                Select grade
                              </option>
                              <option value={DAYCARE_CENTER}>
                                Ages Under 3
                              </option>
                              <option value={KINDERGARTEN}>Ages 3 to 6</option>
                            </select>
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="col-span-1">
                            <div className="flex flex-col">
                              <label htmlFor="firstName">
                                Status<span className="text-rose-500">(*)</span>
                              </label>

                              <select {...register('status')}
                                className="p-2 border rounded-md"
                                disabled={user?.role?.roleName === "Parent"}>
                                <option disabled>Select status</option>
                                {status.map(stas => (
                                  <option key={stas.id} value={stas.id}>{stas.value}</option>
                                ))}
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="grid grid-cols-4 gap-2 flex-1">
                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="lastName">
                              Date of birth{" "}
                              <span className="text-rose-500">(*)</span>
                            </label>

                            <input
                              type="date" max={currentDate}
                              {...register("dateOfBirth", { required: true })}
                              className="p-2 border rounded-md"
                              readOnly={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="lastName">
                              Admission Day
                              <span className="text-rose-500">(*)</span>
                            </label>

                            <input
                              type="date"
                              {...register("admissionDay", { required: true })}
                              className="p-2 border rounded-md"
                              readOnly={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>

                        <div className='col-span-1'>
                          <div className='flex flex-col'>
                            <label htmlFor="address">Ethnic Groups</label>
                            <select {...register('ethnicGroups')}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}>
                              <option disabled>Select Ethnic Groups</option>
                              {ethnicGroup.map(eg => (
                                <option key={eg.id} value={eg.id}>{eg.value}</option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className='col-span-1'>
                          <div className='flex flex-col'>
                            <label htmlFor="address">Nationality</label>
                            <select {...register('nationality')}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}>
                              <option disabled>Select Nationality</option>
                              {nationality.map(na => (
                                <option key={na.id} value={na.id}>{na.value}</option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className='col-span-1'>
                          <div className='flex flex-col'>
                            <label htmlFor="address">Religion</label>

                            <select {...register('religion')}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}>
                              <option disabled>Select Religion</option>
                              {religion.map(re => (
                                <option key={re.id} value={re.id}>{re.value}</option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="phoneNumber">Identifier</label>
                            <input
                              id=""
                              {...register("identifier")}
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                              readOnly={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="lastName">Issue Date</label>

                            <input
                              type="date" max={currentDate}
                              {...register("issueDate")}
                              className="p-2 border rounded-md"
                              readOnly={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="email">Issue Place</label>
                            <input
                              id=""
                              {...register("issuePlace")}
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                              readOnly={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col"></div>
                        </div>
                      </div>
                    </div>

                    <p className="text-[#0497D8] uppercase">
                      2. Permanent Address Information
                      <span className="text-rose-500">(*)</span>
                    </p>
                    <div className="flex flex-col gap-4">
                      <div className="grid grid-cols-4 gap-2 flex-1">
                        <div className="col-span-2">
                          <div className="flex flex-col gap-2">
                            <label htmlFor="firstName">Province</label>
                            <select
                              {...register("province")}
                              onChange={handleProvinceChange}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}
                            >
                              <option disabled>Select Province</option>
                              <option disabled>Select {province}</option>
                              {listProvince?.map(p => (
                                <option key={p.Id} value={p.Id} selected={p.Id == province}>
                                  {p.Name}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col gap-2">
                            <label htmlFor="firstName">District</label>
                            <select
                              {...register("district")}
                              onChange={handleDistrictChange}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}
                            >
                              {listDistrict?.map((d) => (
                                <option key={d.Id} value={d.Id} selected={d.Id == district}>
                                  {d.Name}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col gap-2">
                            <label htmlFor="firstName">Ward</label>
                            <select
                              {...register("subDistrict")}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}
                            >
                              {listWard?.map((w) => (
                                <option key={w.Id} value={w.Id} selected={w.Id == ward}>
                                  {w.Name}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                      </div>

                      <label htmlFor="firstName">Village</label>
                      <input
                        {...register("village")}
                        className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                        readOnly={user?.role?.roleName === "Parent"}
                      />
                    </div>
                  </Tab.Panel>

                  <Tab.Panel
                    className={classNames(
                      "rounded-xl bg-white p-3",
                      "ring-white/60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2"
                    )}
                  >
                    <p className="text-[#0497D8] uppercase">
                      3. Student Detail Information
                    </p>
                    <div className="flex flex-col gap-4">
                      <div className="grid grid-cols-4 gap-2 flex-1">
                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="">Nick Name</label>
                            <input
                              {...register("nickName")}
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                              readOnly={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="">
                              Disabled Type
                            </label>
                            <select {...register('isDisabled')}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}>
                              <option>Select Disabilities Type</option>
                              {disabilities?.map(dis => (
                                <option key={dis.id} value={dis.id}>{dis.value}</option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="">
                              Ethnic MiNority Father
                            </label>
                            <input
                              {...register("ethnicMinorityFather")} type='checkbox'
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded" value={true}
                              disabled={user?.role?.roleName === "Parent"} />
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="">
                              Ethnic MiNority Mother
                            </label>
                            <input
                              {...register("ethnicMinorityMother")} type='checkbox'
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded" value={true}
                              disabled={user?.role?.roleName === "Parent"} />

                          </div>
                        </div>
                      </div>

                      <div className="grid grid-cols-4 gap-2 flex-1">
                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="">Two Session/day</label>
                            <input
                              {...register("isTwoSession")}
                              type="checkbox"
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                              value={true}
                              readOnly={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>
                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="">
                              Beneficiary Of SocialWelfare
                            </label>
                            <input
                              {...register("isBeneficiaryOfSocialWelfare")}
                              type="checkbox"
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                              value={true}
                              disabled={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>
                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="">Swim</label>
                            <input
                              {...register("isCanSwim")}
                              type="checkbox"
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                              value={true}
                              disabled={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>
                        <div className="col-span-1">
                          <div className="flex flex-col">
                            <label htmlFor="">Day Boarding</label>
                            <input
                              {...register("isDayBoarding")}
                              type="checkbox"
                              className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                              value={true}
                              disabled={user?.role?.roleName === "Parent"}
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <p className="text-[#0497D8] uppercase">
                      4. Permanent Address Information
                    </p>
                    <div className="flex flex-col gap-4">
                      <div className="grid grid-cols-4 gap-2 flex-1">
                        <div className="col-span-2">
                          <div className="flex flex-col gap-2">
                            <label htmlFor="firstName">Province</label>

                            <select
                              {...register("province2")}
                              onChange={handleProvinceChange2}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}
                            >
                              <option>Select Province</option>
                              {listProvince?.map((province) => (
                                <option key={province.Id} value={province.Id} selected={province.Id == province2} >
                                  {province.Name}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col gap-2">
                            <label htmlFor="firstName">District</label>
                            <select
                              {...register("district2")}
                              onChange={handleDistrictChange2}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}
                            >
                              <option>Select District</option>
                              {listDistrict2?.map((district) => (
                                <option key={district.Id} value={district.Id} selected={district.Id == district2}>
                                  {district.Name}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>

                        <div className="col-span-1">
                          <div className="flex flex-col gap-2">
                            <label htmlFor="">Ward</label>
                            <select
                              {...register("subDistrict2")}
                              className="p-2 border rounded-md"
                              disabled={user?.role?.roleName === "Parent"}
                            >
                              <option>Select Ward</option>
                              {listWard2?.map((ward) => (
                                <option key={ward.Id} value={ward.Id} selected={ward.Id == ward2}>
                                  {ward.Name}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                      </div>

                      <label htmlFor="">Place Of Birth</label>
                      <input
                        {...register("placeOfBirth")}
                        className="px-2 py-2 font-thin border border-[#ced4da] rounded"
                        readOnly={user?.role?.roleName === "Parent"}
                      />
                    </div>
                  </Tab.Panel>
                </Tab.Panels>
              </Tab.Group>

              {user?.role?.roleName !== "Parent" && (
                <div className="flex justify-end">
                  <button
                    type="submit"
                    className="w-48 p-2 bg-gradient-to-r from-cyan-500 to-blue-500 text-white font-bold py-2 px-4 rounded"
                  >
                    UPDATE
                  </button>
                </div>
              )}

            </form>
          </div>
        </main>
      </div>
    </>
  );
};

export default CreateStudent;
