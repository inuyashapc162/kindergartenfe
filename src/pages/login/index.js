import axios from "axios";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import { useForm } from "react-hook-form";

export default function Login() {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm();

  console.log("🚀 ========= errors:", errors);
  const onSubmit = (data) => {
    console.log(data);
    handleLogin(data?.username, data?.password);
  };

  const handleLogin = async (username, password) => {
    try {
      const accessToken = await axios.post(
        `${process.env.NEXT_PUBLIC_API_URL}/accounts/login`,
        {
          username,
          password,
        }
      );
      console.log("🚀 ========= accessToken:", accessToken);
      if (accessToken?.data?.data?.token) {
        localStorage.setItem("token", JSON.stringify(accessToken?.data?.data));
        router.push("/home");
      }
    } catch (error) {
      setError("username", { message: error.toString() });
      console.log(errors);
    }
  };

  return (
    <section className="bg-gray-50 dark:bg-gray-900">
      <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
        <a
          href="#"
          className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
        >
          <Image
            className="w-8 h-8 mr-2"
            src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/logo.svg"
            width={100}
            height={100}
            alt="logo"
          />
          Kindergarten Management
        </a>
        <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
          <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
            <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
              Sign in to your account
            </h1>
            <form
              onSubmit={handleSubmit(onSubmit)}
              className="space-y-4 md:space-y-6"
            >
              {/* register your input into the hook by invoking the "register" function */}
              <label
                htmlFor="username"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Username
              </label>
              <input
                {...register("username", { required: true })}
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                placeholder="name@company.com"
              />
              {errors.username && (
                <span className="text-red-500">
                  Username are incorrect or invalid
                </span>
              )}
              {/* include validation with required or other standard HTML validation rules */}
              <label
                htmlFor="password"
                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
              >
                Password
              </label>
              <input
                type="password"
                {...register("password", { required: true })}
                placeholder="••••••••"
                className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              />
              {/* errors will return when field validation fails  */}
              {errors.password && (
                <span className="text-red-500">
                  Password are incorrect or invalid
                </span>
              )}

              <button
                type="submit"
                className="w-full bg-blue-500 text-white rounded-lg text-sm px-5 py-2.5 text-center"
              >
                Sign in
              </button>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}
