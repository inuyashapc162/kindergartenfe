import Navbar from "@/components/Navbar";
import React, { useEffect, useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useRouter } from "next/router";
import axios from "axios";
import decodeToken from "@/utils/auth";
import { toast } from "react-toastify";


const getMonthName = (date) => {
  const monthNames = [
    "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  if (date) {
    const [year, month, day] = date.toString().split('-').map(Number);
    const monthIndex = month - 1;
    return monthNames[monthIndex];
  }
};

const Attendance = () => {
  const router = useRouter();
  const [listStudent, setListStudent] = useState([]);
  const [classID, setClassID] = useState("");
  const [currentDate, setCurrentDate] = useState("");
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [attendanceData, setAttendanceData] = useState([]);
  const [attendanceDataByMonth, setAttendanceDataByMonth] = useState([]);
  const [selectedStatus, setSelectedStatus] = useState({});
  const [role, setRole] = useState({});

  const getClassID = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    setClassID(decode?.decoded?.data?.user?.classID);
    setRole(decode?.decoded?.data?.user?.role?.roleName);
  };

  useEffect(() => {
    getClassID();
    setCurrentDate(new Date().toISOString().split("T")[0]);
    setSelectedDate(new Date().toISOString().split("T")[0]);
  }, []);

  const getAllStudent = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    const user = decode?.decoded?.data?.user;
    try {
      const result = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/students`,
        {
          params: user,
        }
      );
      setListStudent(result?.data?.data?.result);
    } catch (error) {
      console.log("error:", error);
    }
  };

  const handleDateChange = (date) => {
    setSelectedDate(new Date(date).toISOString().split("T")[0]);
  };

  const getAttendanceByMonth = async (date) => {
    try {
      const result = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/attendanceDetail/getByMonth`,
        {
          params: { date },
        }
      );
      setAttendanceDataByMonth(result?.data?.data);
    } catch (error) {
      console.log("error:", error);
    }

  };

  const getAttendanceData = async () => {
    try {
      const response = await axios.get(
        `${process.env.NEXT_PUBLIC_API_URL}/attendanceDetail`,
        {
          params: {
            date: currentDate,
          },
        }
      );
      const data = response.data.data;
      setAttendanceData(data);
      if (data) {
        const selectedStatusObj = {};
        data.forEach((item) => {
          selectedStatusObj[item.studentID] = item.status;
        });
        setSelectedStatus(selectedStatusObj);
      } else {
        setSelectedStatus(false)
      }

    } catch (error) {
      console.error("Error fetching attendance data:", error);
    }
  };

  const initializeAttendance = async () => {
    try {
      const body = {
        classID: classID,
        date: currentDate,
      };

      await axios.put(
        `${process.env.NEXT_PUBLIC_API_URL}/attendanceDetail/initializeAttendanceDetail`,
        body
      );
      console.log("Attendance details initialized successfully.");
    } catch (error) {
      console.error("Error initializing attendance details:", error);
    }
  };

  const handleRadioChange = (studentID, status) => {
    setSelectedStatus((prevSelectedStatus) => ({
      ...prevSelectedStatus,
      [studentID]: status,
    }));

    const updatedAttendanceData = attendanceData.map((data) => {
      if (data.studentID === studentID) {
        return { ...data, status: status };
      }
      return data;
    });
    setAttendanceData(updatedAttendanceData);
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();

    try {
      const formattedDate = new Date(currentDate).toISOString().split("T")[0];
      const requests = attendanceData.map((data) => {
        return axios.put(
          `${process.env.NEXT_PUBLIC_API_URL}/attendanceDetail`,
          {
            studentID: data.studentID,
            status: selectedStatus[data.studentID],
            date: formattedDate,
          }
        );
      });

      await Promise.all(requests);
      toast.success("Attendance successfully!", {
        position: "top-right",
        autoClose: 3000, // Đóng tự động sau 3 giây
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
    } catch (error) {
      console.error("Error saving attendance:", error);
    }
  };

  useEffect(() => {
    getAllStudent();
    initializeAttendance();
    getAttendanceData();
    getAttendanceByMonth(currentDate);
  }, [classID, currentDate]);

  useEffect(() => {
    getAttendanceByMonth(selectedDate);
  }, [selectedDate]);

  useEffect(() => {
    const initializedData = listStudent.map((student) => ({
      studentID: student._id,
      status: false,
    }));
    setAttendanceData(initializedData);
  }, [listStudent]);



  return (
    <>
      <Navbar />
      <div className="flex w-full">
        <main className="p-2 bg-[#f0f0f0] h-full min-h-screen flex-1">
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 flex items-center justify-between shadow-md">
            <form className="w-full" onSubmit={handleFormSubmit}>
              <div className="flex items-center justify-between p-2">
                <DatePicker
                  dateFormat="yyyy-MM-dd"
                  showMonthYearPicker
                  className="border px-4 py-2 rounded"
                  placeholderText="Choose date"
                  selected={new Date(selectedDate)}
                  onChange={handleDateChange}
                />
                <div className="border px-4 py-2 rounded">
                  Today{" "}
                  {new Date(currentDate).toLocaleDateString("en-US", {
                    year: "numeric",
                    month: "numeric",
                    day: "numeric",
                  })}
                </div>

                <button
                  type="submit"
                  className="px-4 py-2 bg-green-500 text-white rounded transition duration-300 ease-in-out hover:bg-green-600 transform hover:scale-105"
                  hidden = {role === "Parent"}
                >
                  Save Attendance
                </button>
              </div>
              <table className="w-full border-2 border-gray mx-auto bg-opacity-5">
                <thead className="bg-black bg-opacity-10">
                  <tr>
                    <th className="border border-gray-300 p-2">No</th>
                    <th className="border border-gray-300 p-2">Student name</th>
                    <th className="border border-gray-300 p-2">Attendance</th>
                    <th className="border border-gray-300 p-2">Absent</th>
                  </tr>
                </thead>
                <tbody>
                  {listStudent?.map((item, index) => {
                    const studentName = item?.fullName;
                    const studentID = item?._id;

                    const studentAttendance = attendanceData.find(
                      (data) => data.studentID === studentID
                    );

                    return (
                      <tr key={studentID}>
                        <td className="border border-gray-300 text-center">
                          {index + 1}
                        </td>
                        <td className="border border-gray-300 text-center">
                          {studentName}
                        </td>
                        <td className="border border-gray-300 text-center">
                          <div className="flex items-center justify-center p-1">
                            <input
                              type="radio"
                              value={true}
                              checked={selectedStatus[studentID] === true}
                              onChange={() => handleRadioChange(studentID, true)}
                              disabled={!studentAttendance}
                            />
                          </div>
                        </td>
                        <td className="border border-gray-300 text-center">
                          <div className="flex items-center justify-center p-1">
                            <input
                              type="radio"
                              value={false}
                              checked={selectedStatus[studentID] === false}
                              onChange={() => handleRadioChange(studentID, false)}
                              disabled={!studentAttendance}
                            />
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </form>
          </div>
          <div className="break-words w-11/12 m-auto bg-[#fff] bg-clip-border border rounded p-4 mb-2 shadow-md">
            <h2 className="text-xl font-semibold mb-4">Attendance Data for {getMonthName(selectedDate)}</h2>
            <table className="w-full border-2 border-gray mx-auto bg-opacity-5">
              <thead className="bg-black bg-opacity-10">
                <tr>
                  <th className="border border-gray-300 p-2">No</th>
                  <th className="border border-gray-300 p-2">Student name</th>
                  <th className="border border-gray-300 p-2">Attendance Total In Month</th>
                </tr>
              </thead>
              <tbody>
                {attendanceDataByMonth.length === 0 ? (
                  <tr>
                    <td colSpan="3" className="border border-gray-300 text-center">This month not have data</td>
                  </tr>
                ) : (
                  attendanceDataByMonth.map((item, index) => (
                    <tr key={item.studentID}>
                      <td className="border border-gray-300 text-center">{index + 1}</td>
                      <td className="border border-gray-300 text-center">{item.studentName}</td>
                      <td className="border border-gray-300 text-center">{item.attendanceCount}</td>
                    </tr>
                  ))
                )}
              </tbody>

            </table>
          </div>
        </main>
      </div>
    </>
  );
};

export default Attendance;
