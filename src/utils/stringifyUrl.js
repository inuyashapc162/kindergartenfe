export default function stringifyUrl(params) {
  const paramString = Object.entries(params)
    .map(([key, value]) => {
      if (Array.isArray(value)) {
        return value
          .map((v) => `${encodeURIComponent(key)}=${encodeURIComponent(v)}`)
          .join("&");
      } else {
        return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
      }
    })
    .join("&");

  return paramString;
}

// Explain the code
// The function takes an object of parameters and returns a string of URL parameters.
// It uses the Object.entries method to convert the object into an array of key-value pairs.
// It then uses the map method to iterate over the key-value pairs and create a string for each pair.
// If the value is an array, it iterates over the array and creates a string for each value, joining them with "&".
// If the value is not an array, it creates a string for the key-value pair.
// Finally, it joins all the strings with "&" and returns the resulting string.
// This function can be used to convert an object of parameters into a string that can be appended to a URL. For example:
// const params = { foo: "bar", baz: [1, 2, 3] };
// const paramString = stringifyUrlParams(params);
// console.log(paramString); // "foo=bar&baz=1&baz=2&baz=3"
// This would be useful for constructing URLs with query parameters, such as:
// const url = `https://example.com/api?${paramString}`;
// console.log(url); // "https://example.com/api?foo=bar&baz=1&baz=2&baz=3"
// encodeURIComponet() is used to encode the key and value to make sure that the URL is valid and does not contain any illegal characters.
// such as spaces, question marks, and ampersands. This is important for creating valid URLs that can be used to make requests to a server.
