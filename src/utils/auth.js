import axios from "axios";

const decodeToken = async (token) => {
  try {
    const response = await axios.get(
      `${process.env.NEXT_PUBLIC_API_URL}/accounts/decode`,
      {
        headers: {
          token: token, // Sử dụng tiền tố "Bearer " và truyền token qua tiêu đề Authorization
        },
      }
    );
    return response.data;
  } catch (error) {
    console.error("Error decoding token:", error);
    return null;
  }
};

export default decodeToken;
