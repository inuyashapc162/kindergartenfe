import React from 'react';
import customer_module from '../../public/img/customer_module.png';
import Image from 'next/image';

const Sidebar = () => {
  return (
    <aside className="group w-16 bg-white overflow-hidden transition-all max-w-[300px] min-h-[80vh] hover:w-[260px]">
      <a className="block py-3 px-2 text-xl whitespace-nowrap no-underline" href="#">
        <Image className='float-left w-auto leading-3 max-h-8 ml-3 mr-3 mt-[-3px] align-middle border-none' src={customer_module} alt="logo" />
        <span className="text-[#0697d8]">Quản lý học sinh</span>
      </a>
      <div className="py-0 px-2 flex-1">
        <nav className="mt-2">
          <ul className="relative flex flex-col flex-wrap pl-0 mb-0 list-none">
            <li className="mb-0 mr-2">
              <a className=" text-white p-2 relative mb-1 bg-[#1897d8] shadow-none rounded block items-center hover:bg-[#156e9a] group-hover:translate-x-1 group-hover:shadow-md transition-all">
                <i className="fa fa-home menu-icon w-8 text-center font-extrabold inline-block leading-4" />
                <p className="ml-1 hidden group-hover:inline-block">Hồ sơ học sinh</p>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </aside>
  );
};

export default Sidebar;
