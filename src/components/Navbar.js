import React, { useEffect, useState } from "react";
import Homepage from "../../public/svg/Homepage";
import Link from "next/link";
import Logout from "../../public/svg/Logout";
import { useRouter } from "next/router";
import decodeToken from "@/utils/auth";

const Navbar = () => {
  const router = useRouter();
  const [token, setToken] = useState();
  const getAccessToken = async () => {
    const token = localStorage.getItem("token");
    const decode = await decodeToken(JSON.parse(token)?.token);
    console.log("🚀 ========= decode:", decode);
    setToken(decode?.decoded?.data);
  };

  useEffect(() => {
    getAccessToken();
  }, []);

  const handleLogout = () => {
    localStorage.removeItem("token");
    router.push("/");
  };
  return (
    <div className="w-full">
      <nav className="z-999 border-b border-[#dee2e6] bg-white justify-between relative flex items-center p-2">
        <div className="flex items-center">
          <ul className="flex flex-row p-0 m-0 list-none">
            <li>
              <a className="px-4 relative h-10" href="#">
                <i className="fa fa-bars"></i>
              </a>
            </li>
          </ul>
          <div className="whitespace-nowrap ">
            <div className="mr-4 relative inline-block">
              <Link
                className="flex py-2 relative px-4 h-10 align-middle gap-1 text-center bg-gray-50 border rounded-md border-gray-200"
                href={"/home"}
              >
                <Homepage />
                Trang chủ
              </Link>
            </div>
          </div>
        </div>
        {token ? (
          <div className="flex">
            <Link
              href={`/admin/changeProfile/${token?.user?._id}`}
              className="flex py-2 relative px-4 h-10 align-middle gap-1 text-center bg-gray-50 border rounded-md border-gray-200 mr-2"
            >
              {token?.username}
            </Link>
            <div
              className="flex py-2 relative px-4 h-10 align-middle gap-1 text-center bg-gray-50 border rounded-md border-gray-200"
              onClick={handleLogout}
            >
              <Logout />
            </div>
          </div>
        ) : (
          <Link
            href={"/login"}
            className="flex py-2 relative px-4 h-10 align-middle gap-1 text-center bg-gray-50 border rounded-md border-gray-200"
          >
            Login
          </Link>
        )}
      </nav>
    </div>
  );
};

export default Navbar;
