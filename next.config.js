/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
};
const { config } = require("dotenv");
const { parsed } = config();
module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["res.cloudinary.com"],
  },
  env: {
    SECRET_KEY: parsed.SECRET_KEY,
    SECRET_KEY_JWT: parsed.SECRET_KEY_JWT,
  },
};
// module.exports = nextConfig;
